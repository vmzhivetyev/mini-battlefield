﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class BotSpawner : MonoBehaviour 
{
	public static BotSpawner shared;
	[System.Serializable]
	public struct BotInf
	{
		public int id;
		public float deathTime;
	}

	public float respawnTime = 3;
	public float noactiveTime = 60;

	
	public List<BotInf> respawnList = new List<BotInf>();
	//public BotInf[] c;

	void Awake () 
	{
		if(shared)
		{
			Destroy(gameObject);
			return;
		}

		shared = this;

		EventSystem.OnGamePreStart += OnGamePreStart;
	}

	void OnGamePreStart()
	{
		//respawnList.Clear ();
	}



	void Update () 
	{
		if (!Network.isServer) return;

		//c = respawnList.ToArray ();
		if(Game.state != GameState.InGame) 
		{
			respawnList.Clear();
			return;
		}

		//if(Time.time-Game.shared.lastSpawn)

		foreach(BotInf b in respawnList)
		{
			if(b.deathTime + respawnTime <= Time.time)
			{
				Bot.SpawnBot(b.id);
			}
		}
		respawnList.RemoveAll (x => x.deathTime + respawnTime <= Time.time);
	}
}
