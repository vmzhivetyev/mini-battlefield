﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DebugText : MonoBehaviour 
{
	public Text text;
	public Car c { get { return GetComponent<Car> (); } }
	public Player b { get { return GetComponent<Player> (); } }

	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(c) text.text = Mathf.Ceil(c.healthPoints / c.maxHealthPoints * 100).ToString();
		if(b) text.text = Mathf.Ceil(b.healthPoints / b.maxHealthPoints * 100).ToString();
			//string.Format ("{0:0.00}", rigidbody2D.velocity.magnitude);
	}
}
