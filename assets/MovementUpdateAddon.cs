﻿using UnityEngine;
using System.Collections;

public class MovementUpdateAddon : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(GetComponent<MovementUpdate>().enabled == false && !GetComponent<NetworkView>().isMine)
		{
			transform.rotation = GetComponent<MovementUpdate>().newRotation2;
		}
	}
}
