﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System.Collections.Generic;

public class Bot : MonoBehaviour 
{
	public float viewDistance;
	
	public GameObject nearest;
	public bool attackingNow = false;
	public bool kicked = false;
	public LayerMask layerMask;

	public Player player { get { return GetComponent<Player> (); } }
	
	#region static

	public static int botsCount;
	public static int botsTotal;//is NOT COUNT of bots in scene  // и как чекать?
	public static int getNextBotId { get { return int.MaxValue - botsTotal; } }

	public static void AddBots(int count)
	{
		for(int i=0; i<count; i++)
		{
			AddBot();
		}
	}

	public static void AddBot()
	{
		if (!Network.isServer) return;
		NetworkManager.shared.GetComponent<NetworkView>().RPC("ReceiveNewPlayerInfo", RPCMode.AllBuffered, "Bot", getNextBotId, "_bot", 0);
		BotSpawner.shared.respawnList.Add (new BotSpawner.BotInf{id = getNextBotId, deathTime = 0});
		Debug.Log ("Add bot " + (int.MaxValue - getNextBotId));

		botsTotal++;
	}

	public static void SpawnBot(int _id)
	{
		if (!Network.isServer) return;

		Debug.Log("Spawn bot "+(int.MaxValue - _id));

		NetworkPlayerInfo np = NetworkManager.shared.PlayerList.Find(x => x.id == _id);
		if(np.gameObject)
			return;

		GameObject[] spawnPoints = GameObject.FindGameObjectsWithTag("PlayerSpawnPoint");
		//spawnPoints = GameObject.FindGameObjectsWithTag("PlayerSpawnPoint"+NetworkManager.MyInfo.team);

		int rand = Random.Range(0, spawnPoints.Length);
		GameObject b = Network.Instantiate(SpawnScript.shared.PlayerPrefab, spawnPoints[rand].transform.position, spawnPoints[rand].transform.rotation, 0) as GameObject;
		Bot bc = b.AddComponent<Bot>();
		b.GetComponent<Player> ().id = _id;
	}

	public static bool KickBot(int _id)
	{
		if (!Network.isServer) return false;

		int index = NetworkManager.shared.PlayerList.FindIndex (x => x.id == _id);

		if(index < 0)return false;

		NetworkPlayerInfo np = NetworkManager.shared.PlayerList[index];

		if(np.gameObject)
		{
			Player p = np.gameObject.GetComponent<Player> ();
			p.GetComponent<Bot> ().kicked = true;
			p.SetDamagerID (-1);
			p.ApplyDamage (p.healthPoints);
		}

		NetworkManager.shared.GetComponent<NetworkView>().RPC("PlayerLeft", RPCMode.AllBuffered, _id);
		Debug.Log ("Kick bot " + (int.MaxValue - _id));
		return true;
	}

	public static void KickBots(int count = int.MaxValue)
	{
		if (!Network.isServer) return;
		Debug.Log ("Kick " + count + " bots");
		int left = count;
		for(int i = getNextBotId+1; i > 0; i++)
		{
			if(left == 0)return;
			if(KickBot(i))
				left--;
		}
	}

	public static void CheckBotsCount()
	{
		if(!Network.isServer)return;

		if(Game.state != GameState.InGame) return;

		if(Game.current.bots == BotsCountType.None)
			Game.current.botsNeeded = 0;

		if(Game.current.botsNeeded == 0)
			return;

		if(Game.current.bots == BotsCountType.Quota)
		{
			int d = NetworkManager.shared.PlayerList.Count - Game.current.botsNeeded;
		
			if(d > 0)
			{
				KickBots(d);//recursively d
				Debug.Log("CheckBotsCount kick "+(d));
			}
			else if(d < 0)
			{
				AddBots(-d);//recursively -d
				Debug.Log("CheckBotsCount add "+(-d));
			}
		}

		if(Game.current.bots == BotsCountType.ConstantCount)
		{

		}
	}
	
	#endregion

	void Awake()
	{
		if (!Network.isServer) return;
		gameObject.AddComponent<Seeker>();
		gameObject.AddComponent<BotPathfinding>();
	}
	
	void Start () 
	{
		if(!Network.isServer)
		{
			enabled = false;
			return;
		}

		//gameObject.tag = "Bot";
		layerMask = GetComponent<ShootingController> ().bulletPrefab.GetComponent<BulletScript> ().botLayerMask;
		attackingNow = false;
		kicked = false;

		if(viewDistance == 0)
			viewDistance = Camera.main.orthographicSize;
	}

	void OnDestroy()
	{
		if (!Network.isServer) return;

		if(!kicked)
			BotSpawner.shared.respawnList.Add (new BotSpawner.BotInf{id = player.id, deathTime = Time.time});
	}

	void Update () 
	{
		ShootNearestPlayer ();
		Move ();
	}

	void OnDrawGizmos()
	{
		Gizmos.color = Color.yellow;
		Gizmos.DrawWireSphere (transform.position, viewDistance);
	}

	#region moving

	public GameObject target;
	public Vector3 t;

	void Move()
	{
		t = GetComponent<BotPathfinding> ().GetNextPoint (nearest);

		if(!nearest || (!CantRaycast(nearest) && 2*Vector2.Distance((Vector2)transform.position, (Vector2)nearest.transform.position) < viewDistance))
			SetMovingDirection(Vector3.zero);
		else 	if(t == -Vector3.one)
					SetMovingDirection(Vector3.zero);
				else
					SetMovingDirection(t - transform.position);
	}

	void SetMovingDirection(Vector3 dir)
	{
		GetComponent<PlayerController> ().x = dir.x;
		GetComponent<PlayerController> ().y = dir.y;
	}

	#endregion

	#region shooting
	
	void ShootNearestPlayer()
	{
		nearest = GetNearestPlayer();
		
		if(!nearest || CantSee(nearest.transform.position) || CantRaycast(nearest))
		{
			attackingNow = false;
			return;
		}
		
		attackingNow = true;
		Vector3 p = nearest.transform.position - transform.position; p.z = 0;
		transform.LookAt (transform.position + new Vector3 (0, 0, 1), p);
	}
	
	GameObject GetNearestPlayer()
	{
		var arr = GameObject.FindGameObjectsWithTag("Player").ToList<GameObject>();
		
		arr.Remove(gameObject);
		//arr.RemoveAll(x => CantSee(x.transform.position));
		//arr.RemoveAll(x => CantRaycast(x));
		
		//arr.Sort((x,y) => Vector2.Distance((Vector2)x.transform.position, (Vector2)transform.position) < Vector2.Distance((Vector2)y.transform.position, (Vector2)transform.position));
		if(arr.Count == 0)
			return null;
		
		GameObject t = arr[0];
		foreach(GameObject x in arr)
			if(Vector2.Distance((Vector2)x.transform.position, (Vector2)transform.position) < Vector2.Distance((Vector2)t.transform.position, (Vector2)transform.position))
				t = x;
		
		return t;
	}

	bool CantSee(Vector3 pos)
	{
		return Vector2.Distance ((Vector2)pos, (Vector2)transform.position) > viewDistance;
	}

	bool CantRaycast(GameObject go)
	{
		List<RaycastHit2D> hits = (Physics2D.RaycastAll (transform.position, 
		                                        go.transform.position - transform.position, 
		                    					Vector2.Distance (transform.position, go.transform.position), 
		                                        layerMask.value)).ToList<RaycastHit2D>();

		hits.RemoveAt (0);
		//hits.RemoveAll(x => x.collider.gameObject.tag == "Car");

		if(hits.Count == 0)
			return true;

		RaycastHit2D hit = hits [0];
		if(hit.collider.gameObject.tag == "Car")
		{
			return !hit.collider.gameObject.GetComponent<CarNetwork>().IsPassenger(go);
		}

		return hit.collider.gameObject != go;
	}

	#endregion
}























