﻿using UnityEngine;
using System.Collections;
using Pathfinding;
using System.Linq;
using System.Collections.Generic;

public class BotPathfinding : MonoBehaviour 
{
	public Vector3 targetPosition = -Vector3.one;
	public Path p;
	public float pointSkipRange = 0.5f;
	
	Seeker s;
	bool mayRegen = true;
	Vector3 prevTarget;

	void Start () 
	{
		s = GetComponent<Seeker> ();
	}

	void Update () 
	{
		
	}

	void GeneratePath()
	{
		mayRegen = false;
		s.StartPath (transform.position, targetPosition, OnPathComplete);
	}

	void OnPathComplete(Path _p)
	{
		p = _p;
		prevTarget = targetPosition;
		mayRegen = true;
	}

	public Vector3 GetNextPoint(GameObject g = null)
	{
		if(g)
			targetPosition = g.transform.position;

		if(targetPosition == -Vector3.one)
			return targetPosition;

		if(mayRegen && targetPosition != prevTarget)
			GeneratePath();

		if(p == null)
			return -Vector3.one;

		//List<Vector3> l = p.vectorPath.SkipWhile (x => Vector2.Distance ((Vector2)transform.position, (Vector2)x) < pointSkipRange).ToList<Vector3>();
		p.vectorPath.RemoveAll (x => Vector2.Distance ((Vector2)transform.position, (Vector2)x) < pointSkipRange);

		if(p.vectorPath.Count == 0)
			return -Vector3.one;

		return p.vectorPath[0];
	}
}
