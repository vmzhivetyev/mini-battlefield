﻿using UnityEngine;
using System.Collections;

public class SetSortingLayer : MonoBehaviour 
{
	public string layer = "Car";
	public LayerTarget target;

	public enum LayerTarget
	{
		trailRenderer,
		textRenderer
	}

	void Start () 
	{
		if(target == LayerTarget.trailRenderer)
			GetComponent<TrailRenderer>().sortingLayerName = layer;
		else
			GetComponent<MeshRenderer>().sortingLayerName = layer;
	}
}
