﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour 
{
	public ClassName currentClass { get { return GetComponent<NetworkView>().isMine ? SpawnScript.shared.currentPlayerClassIndex : _currentClass; } set { _currentClass = value; } }
	private ClassName _currentClass;

	//public Bot bot { get { return GetComponent<Bot> (); } }
	public bool isBot { get { return GetComponent<Bot> (); } }

	public int id = -1;
	public int team { get { return NetworkManager.shared.PlayerList.Find (x => x.id == id).team; } }
	public float healthPoints = -1;
	public int lastDamager = -1;
	public float maxHealthPoints { get { return PlayerClasses.shared.classes[(int)currentClass].maxHealthPoints; } }
	public float timeToBeginRegen { get { return GameSettings.timeToBeginRegen; } }
	public PlayerSoundSystem soundSystem { get { return GetComponent<PlayerSoundSystem>(); } }

	public SpriteRenderer sprite;
	public Color mySpriteColor;

	public GameObject bloodPref;

	private float lastDamageTime = 0;
	private bool regenerating = false;

	void Awake()
	{
		//currentClass = Mathf.Clamp(currentClass, 0, PlayerClasses.shared.classes.Count);
	}

	void Start () 
	{
		if(GetComponent<NetworkView>().isMine)
		{
			healthPoints = maxHealthPoints;
			if(!isBot)
			{
				gameObject.layer = 8;
				Camera.main.transform.position = new Vector3(transform.position.x, transform.position.y, Camera.main.transform.position.z);
				Camera.main.GetComponent<CameraFollow>().goal = transform;
				sprite.color = mySpriteColor;
				id = NetworkManager.MyInfo.id;
			}
			GetComponent<NetworkView>().RPC("SyncInfo", RPCMode.AllBuffered, id, healthPoints, (int)currentClass, team);
		}
	}

	void Update()
	{
		if(healthPoints <= 0)return;

		if(Network.isServer)
		{
			if(Input.GetKeyDown(KeyCode.Alpha0) && GetComponent<NetworkView>().isMine && !isBot)
				ApplyDamage(5);
			if(Input.GetKeyDown(KeyCode.Minus) && GetComponent<NetworkView>().isMine && !isBot)
				ApplyDamage(50);

			if(lastDamageTime + timeToBeginRegen < Time.time)
				GetComponent<NetworkView>().RPC("Regen", RPCMode.All, id);
		}
		if(regenerating)
			healthPoints = Mathf.Clamp(healthPoints + Time.deltaTime * GameSettings.hpRegenSpeed, 0, maxHealthPoints);
	}

	[RPC]
	void Regen(int recid)
	{
		if(recid != id)return;
		//healthPoints = Mathf.Clamp(healthPoints + change, 0, maxHealthPoints);
		regenerating = true;
	}

	[RPC]
	void SyncInfo(int myid, float hp, int myClass, int _team)
	{
		id = myid;
		//team = _team;
		healthPoints = hp;
		currentClass = (ClassName)myClass;
		int i = NetworkManager.shared.PlayerList.FindIndex(x => x.id == id);
		if(i == -1)return;
		NetworkPlayerInfo inf = NetworkManager.shared.PlayerList[i];
		inf.gameObject = gameObject;
		NetworkManager.shared.PlayerList[i] = inf;
		//sprite.color = SpawnScript.shared.playerTeamColors[team];
	}

	public void SetDamagerID(int idtoset)
	{
		lastDamager = idtoset;
	}

	public void ApplyDamage(float damage)
	{
		if(healthPoints <= 0)return;
		GetComponent<NetworkView>().RPC("NetworkDamage", RPCMode.All, id, damage, lastDamager);
	}

	[RPC]
	void NetworkDamage(int receiverId, float damage, int damager)
	{
		if(healthPoints <= 0)return;
		regenerating = false;
		lastDamageTime = Time.time;
		CreateParticle();
		if(receiverId != id) //like if(networkView.isMine
			return;
		soundSystem.GetDamage();
		lastDamager = damager;
		healthPoints = Mathf.Clamp (healthPoints - damage, 0, maxHealthPoints);
		if (healthPoints <= 0)
		{
			Die();
		}
		else
		{
			GetComponent<NetworkView>().RPC("SyncHPRPC", RPCMode.AllBuffered, healthPoints);
		}
	}

	[RPC]
	void SyncHPRPC(float _hp)
	{
		healthPoints = _hp;
	}

	void OnDestroy()
	{
		if ((!Network.isClient && !Network.isServer) || (GetComponent<NetworkView>().isMine && !isBot)) SpawnScript.shared.SetSpawned (false);

		if(GetComponent<NetworkView>().isMine && !isBot)
			Camera.main.orthographicSize = GameSettings.DefaultCamOrtoSize;
	}

	void Die()
	{
		try
		{
			//if(lastDamager != -10)
			{
				NetworkManager.shared.GetComponent<NetworkView>().RPC("OnKill", RPCMode.AllBuffered, id, lastDamager);
				if(lastDamager < 0 || lastDamager == id)
				{
					Chat.shared.OnDeathChat(NetworkManager.shared.PlayerList.Find(x => x.id == id));
				}
				else
				{
					NetworkPlayerInfo killer = NetworkManager.shared.PlayerList.Find(x => x.id == lastDamager); killer.id = lastDamager;
					NetworkPlayerInfo victim = NetworkManager.shared.PlayerList.Find(x => x.id == id); victim.id = id;
					Chat.shared.OnKillChat(killer, victim);
					//Chat.shared.OnKillChat(NetworkManager.shared.PlayerList.Find(x => x.id == id), NetworkManager.shared.PlayerList.Find(x => x.id == lastDamager));
					//print (lastDamager);
				}
			}
		}
		catch{}
		//Network.Destroy (gameObject);
		Network.RemoveRPCs(GetComponent<NetworkView>().viewID);
		//networkView.RPC ("NetworkDestroyPlayer", RPCMode.AllBuffered);
		//SpawnScript.shared.myPlayer = null;
		//SpawnScript.shared.SetSpawned (false);
		Network.Destroy(GetComponent<NetworkView>().viewID);
	}

	/*
	[RPC]
	void NetworkDestroyPlayer()
	{
		gameObject.SetActive (false);
	}*/

	void CreateParticle()
	{
		if(bloodPref)
			Instantiate (bloodPref, transform.position, transform.rotation);
	}
}
