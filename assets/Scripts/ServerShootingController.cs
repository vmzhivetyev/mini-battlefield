﻿using UnityEngine;
using System.Collections;

public class ServerShootingController : MonoBehaviour 
{
	public static ServerShootingController shared;
	public GameObject bulletPrefab;

	void Start()
	{
		shared = this;
	}

	[RPC]
	public void OneShot(int shooterId, float x, float y, Vector3 pos, Quaternion rot)
	{
		GameObject go = Network.Instantiate (bulletPrefab, pos, rot, 0) as GameObject;
		go.GetComponent<NetworkView>().RPC("SyncVelocity", RPCMode.AllBuffered, shooterId, x, y);
	}
}
