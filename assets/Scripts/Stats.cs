﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class Stats : MonoBehaviour
{
	public Vector2 margin;
	public Vector2 size;

	private bool tabbtn = false;

	public bool useOldGUI = false;
	
	void OnButtonDown(string name)
	{
		if(name != "StatsButton")return;
		tabbtn = true;
	}
	
	void OnButtonUp(string name)
	{
		if(name != "StatsButton")return;
		tabbtn = false;
	}

	void Start()
	{
		EasyButton.On_ButtonDown += OnButtonDown;
		EasyButton.On_ButtonUp += OnButtonUp;
	}

	void Update()
	{
		GUIPlayerList.shared.show = (!(!(Input.GetKey(KeyCode.Tab) || tabbtn) || Network.peerType == NetworkPeerType.Disconnected))
			||Game.state==GameState.Finished || DoClick.isShowStats;
	}

	void OnGUI()
	{
		if(!useOldGUI)return;
		if(!(Input.GetKey(KeyCode.Tab) || tabbtn) || Network.peerType == NetworkPeerType.Disconnected)return;
		GUILayout.BeginArea (new Rect (margin.x, margin.y, Screen.width - 2 * margin.x, Screen.height - 2 * margin.y));
		{
			GUILayout.BeginHorizontal("box");
			DrawColumn(0);
			DrawColumn(1);
			GUILayout.EndHorizontal();
		}
		GUILayout.EndArea ();
	}

	void DrawColumn(int team)
	{	
		GUILayout.BeginVertical(GUILayout.Width((Screen.width - 2 * margin.x)/2 - 6));
		
		GUILayout.BeginHorizontal("box");
		GUILayout.Label("  ");
		GUILayout.FlexibleSpace();
		GUILayout.Box("K", GUILayout.Width(40));
		GUILayout.Box("D", GUILayout.Width(40));
		GUILayout.Box("S", GUILayout.Width(90));
		GUILayout.Box("Ping", GUILayout.Width(40));
		GUILayout.EndHorizontal();
		GUILayout.Space(5);

		foreach(NetworkPlayerInfo player in NetworkManager.shared.PlayerList.Where(x => x.team == team))
		{
			GUILayout.BeginHorizontal("box");
			GUILayout.Label("  " + player.nickname);
			GUILayout.FlexibleSpace();
			GUILayout.Box(player.kills.ToString(), GUILayout.Width(40));
			GUILayout.Box(player.deaths.ToString(), GUILayout.Width(40));
			GUILayout.Box(player.score.ToString(), GUILayout.Width(90));
			GUILayout.Box(Network.GetAveragePing(Network.player).ToString(), GUILayout.Width(40));
			GUILayout.EndHorizontal();
			
		}
		if(NetworkManager.shared.PlayerList.Where(x => x.team == team).Count() == 0)
		{
			GUILayout.Box("", GUILayout.ExpandWidth(true), GUILayout.Height(30));
		}

		GUILayout.EndVertical();
	}
}
