using UnityEngine;
using System.Collections;

public class Car : MonoBehaviour 
{
	public float maxHealthPoints = -1;
	public float healthPoints = -1;
	public int lastDamager = -1;
	public float minSpeedToDamage = 3;

	public Transform explosionPos;
	public GameObject explosionPref;
	public float explosionDamage;
	public float explosionRadius = 1.5f;

	[HideInInspector]
	public int ownerTeam = 0;

	void OnSerializeNetworkView(BitStream stream, NetworkMessageInfo info) 
	{
		stream.Serialize(ref healthPoints);
	}

	void Start()
	{
		healthPoints = maxHealthPoints;
	}

	void Update()
	{
		if(healthPoints <= 0)
		{
			GetComponent<CarNetwork>().enabled = false;
			GetComponent<CarPhysics>().enabled = false;
			enabled = false;
		}
		if(Input.GetKeyUp(KeyCode.Y) && Network.isServer)
		{
			ApplyDamage(healthPoints);
		}
	}

	void OnDrawGizmos() 
	{
		Gizmos.color = Color.red;
		Gizmos.DrawWireSphere (explosionPos.position, explosionRadius);
	}

	void OnCollisionEnter2D(Collision2D col)
	{
		if(!enabled)return;
		if(!GetComponent<CarPhysics>().controllable)return;
		if(GetComponent<Rigidbody2D>().velocity.magnitude < minSpeedToDamage)return;
		if(col.collider.transform.tag == "Player")
		{
			float damage = col.collider.gameObject.GetComponent<Player>().maxHealthPoints;
			col.collider.SendMessage("SetDamagerID", NetworkManager.MyInfo.id, SendMessageOptions.DontRequireReceiver);
			col.collider.SendMessage("ApplyDamage", damage, SendMessageOptions.DontRequireReceiver);
		}
	}

	public void ApplyDamage(float damage)
	{
		if(healthPoints <= 0)return;
		GetComponent<NetworkView>().RPC("NetworkDamage", RPCMode.All, damage, lastDamager);
	}

	[RPC]
	void NetworkDamage(float damage, int damager)
	{
		if (healthPoints <= 0) return;
		if (!GetComponent<NetworkView>().isMine) return; //|| !GetComponent<CarNetwork>().used) return;

		lastDamager = damager;

		healthPoints = Mathf.Clamp (healthPoints - damage, 0, maxHealthPoints);
		
		if (healthPoints <= 0)
		{
			Die();
		}
	}

	public void SetDamagerID(int _lastDamager)
	{
		lastDamager = _lastDamager;
	}

	void Die()
	{		
		GetComponent<CarNetwork>().enabled = false;
		GetComponent<CarPhysics>().enabled = false;
		enabled = false;

		GetComponent<NetworkView>().RPC ("CreateExplosion", RPCMode.All);

		var cn = GetComponent<CarNetwork>();
		foreach(var go in cn.passengers)
		{
			if(!go) continue;
			var player = go.GetComponent<Player>();
			player.SetDamagerID(lastDamager);
			player.ApplyDamage(player.healthPoints);
		}


		if (lastDamager >= 0) {
			if(NetworkManager.shared.PlayerList.Find(x => x.id == lastDamager).team != ownerTeam)
			NetworkManager.shared.GiveScore (lastDamager, Game.current.pointsForVehicleDestroy, "Vehicle destroyed");
		}


		Network.RemoveRPCs(GetComponent<NetworkView>().viewID);
		Network.Destroy(GetComponent<NetworkView>().viewID);
	}

	[RPC]
	void CreateExplosion()
	{
		ExplosionDamage e = (Instantiate (explosionPref, explosionPos.position, explosionPos.rotation) as GameObject).GetComponent<ExplosionDamage>();
		if(Network.isServer)
		{
			e.damage = explosionDamage;
			e.radius = explosionRadius;
			e.damager = lastDamager;
		}
	}
}
