﻿using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;
using System;

public class Broadcast: MonoBehaviour {
#if UNITY_ANDROID && !UNITY_EDITOR
	[DllImport ("libbroad_shared")]
	private static extern int _Test ();

	[DllImport ("libbroad_shared")]
	private static extern void _StartBroadcast ();

	[DllImport ("libbroad_shared")]
	private static extern void _StartSearching ();

	[DllImport("libbroad_shared")]
	extern static private void _StopSearching();
	
	[DllImport("libbroad_shared")]
	extern static private void _StopBroadcast();
	
	[DllImport("libbroad_shared")]
	extern static private string _GetResult();

#else
	private static int _Test(){return 5;}
	private static void _StartBroadcast (){}
	private static void _StartSearching (){}
	private static void _StopBroadcast (){}
	private static void _StopSearching (){}
	private static string _GetResult (){ return "localhost";}
#endif

	public const string NO_IP="_";
	public static float timer;
	public static bool isBroadcasting;
	public static bool isSearching;
	public static string resultURI;
	public static event Action<string> resultReceived = delegate {};


	static Broadcast()
	{
		GameObject obj = new GameObject ();
		obj.name = "Broadcast";
		obj.AddComponent<Broadcast>();
		DontDestroyOnLoad (obj);
		//obj.hideFlags = HideFlags.HideInHierarchy;
	}

	public static int Test()
	{
		return _Test ();
	}
	public static void StartBroadcast()
	{
		if (isBroadcasting) return;
		_StartBroadcast();
		isBroadcasting=true;
	}
	public static void StartSearching(float timeout=5)
	{
		if (isSearching) return;
		_StartSearching();
		isSearching = true;
		timer = timeout;
	}
	public static void StopBroadcast()
	{
		if (!isBroadcasting) return;
		_StopBroadcast();
		isBroadcasting=false;
	}
	public static void StopSearching()
	{
		if (!isSearching) return;	
		_StopSearching();
		isSearching = false;
		resultReceived("_");
	}
	static void GetResult()
	{
		string str="_";
		str = _GetResult();
		Debug.Log("Searching... ");
		if (str == "_") return;
			resultURI =str;
			isSearching=false;
			resultReceived(resultURI);
			//resultReceived = delegate {};
		Debug.Log("Getresult "+str);
	}
	void Update()
	{
		if(isSearching){
			if(Time.frameCount % 20 == 0)
			{
				GetResult();
			}
			if(timer>0) timer-=Time.deltaTime;
			else StopSearching();
		}
	}

}
