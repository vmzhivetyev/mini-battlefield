﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class GUIPlayerList : MonoBehaviour 
{
	public static GUIPlayerList shared;

	public GameObject list;
	public Transform rightColumn;
	public Transform leftColumn;
	public bool show = false;
	public GameObject rowPref;
	public float delta;

	public List<GUIPlayerListRow> plist = new List<GUIPlayerListRow>();

	public void Create(int id)
	{
		GameObject g = Instantiate(rowPref, rowPref.transform.position, rowPref.transform.rotation) as GameObject;
		g.transform.SetParent(rowPref.transform.parent);
		g.GetComponent<RectTransform>().sizeDelta = rowPref.GetComponent<RectTransform>().sizeDelta;
		g.GetComponent<GUIPlayerListRow>().id = id;
		g.SetActive(true);
		plist.Add (g.GetComponent<GUIPlayerListRow>());
	}
	public void Delete(int id)
	{
		var rows = plist.Where(x=>x.id==id).ToList<GUIPlayerListRow>();
		foreach (var value in rows)
		{
			plist.Remove(value);
			Destroy(value.gameObject);
		}
	}
	public void Clear()
	{
		print ("Clear GUI list");
		var rows = plist;
		foreach (var value in rows)
		{
			Destroy(value.gameObject);
		}
		plist.Clear ();
	}

	void Awake()
	{
		if (GUIPlayerList.shared) 
			Destroy (this);
		else
			shared = this;
	}

	void Start()
	{
		delta = rowPref.GetComponent<RectTransform>().rect.height;
	}

	void Update () 
	{
		list.SetActive(show);

		if(show)
		{
			List<GUIPlayerListRow> rows = FindObjectsOfType<GUIPlayerListRow>().ToList<GUIPlayerListRow>();
			List<GUIPlayerListRow> rowsLeft = rows.Where(x=>x.team == 0).ToList<GUIPlayerListRow>();
			List<GUIPlayerListRow> rowsRight = rows.Where(x=>x.team == 1).ToList<GUIPlayerListRow>();
			rowsLeft.Sort((x, y) => y.score.CompareTo(x.score));
			rowsRight.Sort((x, y) => y.score.CompareTo(x.score));

			float curr = 0;
			foreach(GUIPlayerListRow r in rowsLeft)
			{
				Vector3 lp = r.transform.localPosition;
				r.transform.SetParent(leftColumn);
				r.transform.localPosition = lp;

				Vector2 v = Vector2.zero;
				v.y = curr;
				r.GetComponent<RectTransform>().anchoredPosition = v;
				curr -= delta;
			}
			
			curr = 0;
			foreach(GUIPlayerListRow r in rowsRight)
			{
				Vector3 lp = r.transform.localPosition;
				r.transform.parent = rightColumn;
				r.transform.localPosition = lp;

				Vector2 v = Vector2.zero;
				v.y = curr;
				r.GetComponent<RectTransform>().anchoredPosition = v;
				curr -= delta;
			}
		}
	}
}
