﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;

public enum GameModeName
{
	PVP=0
}

public enum BotsCountType
{
	None,
	ConstantCount,
	Quota
}

[System.Serializable]
public struct GameMode
{
	public GameModeName name;
	
	public int pointsForKill;
	public int pointsForVehicleDestroy;
	public float time;
	public BotsCountType bots;
	public int botsNeeded;

}
[System.Serializable]
public enum GameState
{
	NonGame=0,
	PreStart,
	InGame,
	Finished
}

public class Game : MonoBehaviour 
{
	public static Game shared;
	public static GameMode current;
	public static GameState state;
	int _state;
	public GameModeName mainMode;
	public List<GameMode> gameModes;

	public Text counter;
	public Text startG;
	public Text finishG;

	string _startG;

	public float timeBeforeGame=3;
	public float timeAfterGame=5;
	float timebg;

	void Awake () 
	{
		if (shared) {
			Destroy (this);
			return;
		} 
		else 
			shared = this;


		EventSystem.OnGameStarted += OnGameStarted;
		EventSystem.OnGameEnded += OnGameEnded;
		EventSystem.OnGameLoaded += OnGameLoaded;
		EventSystem.OnGamePreStart += OnGamePreStart;
	}

	#region game states
	
	void OnGamePreStart()
	{
		Debug.Log ("pre");
		state = GameState.PreStart;
		finishB = false;

		if(Network.isServer)
			Bot.KickBots();
	}

	void OnGameStarted()
	{
		Debug.Log ("started");
		state = GameState.InGame;

		if(Network.isServer){
			if(current.bots == BotsCountType.ConstantCount)
				Bot.AddBots(current.botsNeeded);

			lastSpawn=Time.time;
		}
	}

	void OnGameEnded()
	{
		Debug.Log ("ended");
		state = GameState.Finished;
		finishB = true;
	}

	#endregion

	void OnGameLoaded()
	{
		Debug.Log ("loaded");
		counter = GameObject.Find ("CounterGame").GetComponent<Text>();
		startG = GameObject.Find ("StartGame").GetComponent<Text>();
		finishG = GameObject.Find ("FinishGame").GetComponent<Text>();
		_startG = startG.text.Split('\n')[0]+"\n";

		counterB = true;
		startB = false;
		finishB = false;

		if(Network.isServer) StartGame1(mainMode);
	}

	public float lastSpawn;
	[RPC]
	public void OnPlayerSpawned(int id)
	{
		lastSpawn = Time.time;
	}

	bool counterB,startB,finishB;
	void OnSerializeNetworkView(BitStream stream, NetworkMessageInfo info) 
	{
		stream.Serialize(ref current.time);
		stream.Serialize(ref timebg);

		stream.Serialize(ref counterB);
		stream.Serialize(ref startB);
		stream.Serialize(ref finishB);

		stream.Serialize(ref _state);
	}


	void CountingBeforeGame()
	{
		if (timebg == 0) return;

		if(timebg<=0)
		{
			timebg=0;
			startG.text = _startG;
			startB=false;
			if(Network.isServer) StartGame2();
		}
		else 
		{
			startB=true;
			timebg-=Time.deltaTime;			
		}
	}

	void CountingInGame()
	{
		if (current.time == -1 || state!=GameState.InGame) return;
		if(current.time<=0)
		{
			current.time=-1;
			counterB=false;
			RestartGame(timeAfterGame);
		}
		else 
		{
			counterB=true;
			current.time-=Time.deltaTime;			
		}
	}

	void UpdateObjects()
	{
		if(counter)
		{
			if(counter)counter.text = current.time == -1 ? "-- : --" : string.Format("{0:00}:{1:00}",(int)current.time/60,(int)current.time%60);
			if(startG)startG.text = _startG + (Mathf.Ceil(timebg)).ToString();
			
			if(counter){
				counter.enabled=counterB;
				counter.gameObject.transform.parent.gameObject.SetActive(counterB);
			}
			if(startG)startG.enabled=startB;
			if(finishG)finishG.enabled=finishB;
		}

		if (Network.isServer) _state = (int)state;
		if (Network.isClient) state = (GameState)_state;

		Bot.CheckBotsCount ();
	}

	void Update()
	{
		current.botsNeeded = Mathf.Clamp (current.botsNeeded, 0, 10);

		UpdateObjects ();

		if(!Network.isServer)return;

		CountingBeforeGame ();
		CountingInGame ();

	}

	[RPC]
	public void LoadGameMode(int iname)
	{
		print ("LoadGameMode");
		GameModeName _name = (GameModeName)iname;
		current =  shared.gameModes.Find (x => x.name == _name);
		if (current.time <= 0) current.time = -1;
	}

	void GameOver()
	{
		if(!Network.isServer)return;
		EventSystem.GameEnded();
		KillAllPlayers();
		Chat.shared.SendChatMessage ("Game Ended");
	}

	public void RestartGame(float time=3)
	{
		GameOver ();
		Invoke ("StartGame1", time);
	}

	void StartGame1()
	{
		StartGame1 (current.name);
	}

	public void StartGame1(GameModeName name) //этап 1, отсчет
	{
		EventSystem.GamePreStart();
		KillAllPlayers();
		NetworkManager.shared.ResetStats();

		//LoadGameMode ((int)name);
		GetComponent<NetworkView>().RPC ("LoadGameMode", RPCMode.AllBuffered, (int)name);

		timebg = timeBeforeGame;
	}
	
	public void StartGame2() //этап 2, сама игра
	{
		if(!Network.isServer)return;
		EventSystem.GameStarted();
		Chat.shared.SendChatMessage ("Game Started");
	}

	void KillAllPlayers()
	{
		if(!Network.isServer)return;
		foreach(Player p in GameObject.FindObjectsOfType(typeof(Player)))
		{
			p.SetDamagerID(-10);
			p.ApplyDamage(p.healthPoints);
		}
	}
}
