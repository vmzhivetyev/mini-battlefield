﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GUISystemPlayer : MonoBehaviour 
{
	public Canvas mainCanvas;
	public Text hpText;
	public Text vehicleHpText;
	public Text ammoText;

	private int hp { get { return (int)(GetComponent<Player>().healthPoints); } }
	private int carHp { get { return (int)(ce.inCar.GetComponent<Car>().healthPoints / ce.inCar.GetComponent<Car>().maxHealthPoints * 100); } }
	private CarEnter ce { get { return GetComponent<CarEnter>(); } }

	private int bulletsLeft { get { return GetComponent<ShootingController>().currentBullets; } }

	void Start()
	{
		//mainCanvas.transform.parent = null;
		mainCanvas.gameObject.SetActive (GetComponent<NetworkView>().isMine);

		if(GetComponent<Player>().isBot)
			mainCanvas.gameObject.SetActive (false);
		else
			mainCanvas.worldCamera = Camera.main;

		enabled = mainCanvas.gameObject.activeInHierarchy;
	}

	void Update()
	{
		UpdateGUI();
	}
	
	void UpdateGUI()
	{
		if(!GetComponent<NetworkView>().isMine)return;
		
		hpText.text = "+"+hp;
		if(vehicleHpText){
		if(ce.inCar)
		{
			vehicleHpText.text = "VEH:"+carHp;
			if(!vehicleHpText.transform.parent.gameObject.activeInHierarchy) 
				vehicleHpText.transform.parent.gameObject.SetActive(true);
		}
		else if(vehicleHpText.transform.parent.gameObject.activeInHierarchy)
		{
			vehicleHpText.transform.parent.gameObject.SetActive(false);
		}
		}

		ammoText.text = bulletsLeft+"";
	}
}
