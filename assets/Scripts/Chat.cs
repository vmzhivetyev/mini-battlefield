﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class Chat : MonoBehaviour 
{
	public static Chat shared;

	private struct ChatMessage
	{
		public int sender;
		public string text;
		public float deleteTime;
	}

	private string playerName { get { return NetworkManager.MyInfo.nickname; } }

	public int windowLeft = 100;
	public int windowTop;
	public int windowWidth = 300;
	public int windowHeight = 140;

	public float MsgShowSeconds = 5;

	public bool muteUsualPlayers = false;

	private List<ChatMessage> messages = new List<ChatMessage>();

	#region private

	private string messageToSend;
	private string communication;
	
	private bool showTextBox = false;
	private bool sendMessage = false;

	private string lastGUIText;
	
	private Rect windowsRect;

	//private int padding = 20;
	//private int textFieldHeight = 30;
	private Vector2 scrollPosition;
	
	//private bool istyping = false;
	public GUISkin GUIskin;
	private GUIStyle myStyle;

	private Rect chatRect;

	#endregion
	
	void  Awake ()
	{
		if (shared) {
			Destroy (this);
			return;
		} 
		else 
			shared = this;

		Input.eatKeyPressOnTextFieldFocus = false;
		messageToSend = "";	

		windowHeight = Screen.height / 2;
		chatRect = new Rect(windowLeft, Screen.height/2 - windowHeight/2, windowWidth, windowHeight);

		if(GUIskin)
		myStyle = GUIskin.customStyles[0];
	}
	
	void Start()
	{
		scrollPosition.y = int.MaxValue;
	}
	
	void Update()
	{
		if(Network.peerType != NetworkPeerType.Disconnected)
		{
			if(Input.GetKeyDown (KeyCode.T) && showTextBox == false)
			{
				showTextBox = true;
			}
			
			if(Input.GetKeyDown (KeyCode.Return) && showTextBox == true)
			{
				sendMessage = true;
			}

			if(Input.GetKeyDown(KeyCode.Escape))
			{
				showTextBox = false;
				messageToSend = "";
			}
		}
	}
	
	void OnGUI()
	{
		GUI.skin = GUIskin;
		if(Network.peerType != NetworkPeerType.Disconnected)
		{
			//Rect windowRect = new Rect(Screen.width - windowWidth, 0, windowWidth, windowHeight);

			GUILayout.BeginArea(chatRect);

			GUILayout.BeginScrollView(scrollPosition, GUILayout.MaxWidth(windowWidth), GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(true));
			
			for(int i = 0; i < messages.Count; i++)
			{
				ChatMessage msg = messages[i];
				GUILayout.Box((Network.isServer ? (msg.sender + ": ") : "") + msg.text, GUILayout.ExpandWidth(false));
			}
			
			if(showTextBox == true)
			{
				GUI.SetNextControlName("MyTextField");
				messageToSend = GUILayout.TextField(messageToSend, GUILayout.ExpandWidth(true));
				GUI.FocusControl("MyTextField");
				
				if(sendMessage == true)
				{
					
					if(messageToSend != "")
					{
						SendChatMessage(Network.isServer ? "Server" : playerName, messageToSend);
					}
					
					sendMessage = false;
					
					showTextBox = false;
					
					messageToSend = "";
				}
			}

			if(!showTextBox && Network.isClient)
			for(int i = 0; i < messages.Count; i++)
			{
				if(Time.time > messages[i].deleteTime)
				{
					messages.Remove(messages[i]);
					i--;
				}
			}
			
			GUILayout.EndScrollView();

			GUILayout.EndArea();
		}
	}

	public void SendChatMessage(string sender, string msg)
	{
		SendChatMessage(sender + ": " + msg);
	}

	public void SendChatMessage(string msg)
	{
		if(Network.isClient)
			GetComponent<NetworkView>().RPC("_ProcessMsg", RPCMode.Server, NetworkManager.MyInfo.id, msg);
		else
			BroadcastMessage(NetworkManager.MyInfo.id, msg);
	}

	[RPC] //called on server
	private void _ProcessMsg(int senderID, string msg)
	{
		if(muteUsualPlayers)return;

		BroadcastMessage(senderID, msg);
	}

	private void BroadcastMessage(int senderID, string msg)
	{
		GetComponent<NetworkView>().RPC("_ReceiveChatMessageRPC", RPCMode.Others, msg);
		messages.Add(new ChatMessage{ sender = senderID, text = msg, deleteTime = Time.time + MsgShowSeconds });
	}
	
	[RPC]
	void _ReceiveChatMessageRPC(string msg)
	{
		//communication += msg;
		messages.Add(new ChatMessage{ text = msg, deleteTime = Time.time + MsgShowSeconds });
	}

	void TellKillEveryone(string killer, string victim, float distance = 0)
	{
		//communication = killer + " killed " + victim + "\n" + "\n" + communication;
		SendChatMessage(killer + " killed " + victim + (distance == 0 ? "" : " from "+distance.ToString("0")+" meters"));
	}

	void TellDeath(string name)
	{
		//communication = name + " died\n\n" + communication;
		SendChatMessage(name + " Died");
	}

	void TellJoin(string end)
	{
		//communication = name + " Joined." + "\n" + "\n" + communication;
		SendChatMessage(""+playerName + " Joined " + end);
	}

	public void TellLeave(int id)
	{
		//communication = name + " Left." + "\n" + "\n" + communication;
		if(NetworkManager.shared.PlayerList.Any(x => x.id == id))
		BroadcastMessage(id, NetworkManager.shared.PlayerList.First(x => x.id == id).nickname + " Left the game");
	}

	#region events
	
	public void OnKillChat(NetworkPlayerInfo killer, NetworkPlayerInfo victim)
	{
		TellKillEveryone( (!NetworkManager.shared.PlayerList.Any(x => x.id == killer.id)) ? ("Somebody["+killer.id+"]") : killer.nickname, victim.nickname, /*Vector3.Distance(killer.position, victim.position)*/ 0);
	}

	public void OnDeathChat(NetworkPlayerInfo victim)
	{
		TellDeath(victim.nickname);
	}

	//called on client
	void OnConnectedToServer()
	{
		TellJoin("the game");
	}

	public void OnJoinedTeam(string team)
	{
		TellJoin("team "+team);
	}

	void  OnDisconnectedFromServer ( NetworkDisconnection info  )
	{
		messages.Clear ();
	}

	#endregion
}
