﻿using UnityEngine;
using System.Collections;

public class AnimationController : MonoBehaviour {
	public Animator anim;
	public int Speed=0;
	public bool AttackKnife=false;
	public SpriteRenderer sr;

	void Animate()
	{
		anim.SetInteger("Speed",Speed);
		anim.SetBool("AttackKnife",AttackKnife);
	}
	void OnSerializeNetworkView(BitStream stream, NetworkMessageInfo info) 
	{
		stream.Serialize(ref Speed);
		stream.Serialize(ref AttackKnife);
	}
	void Update () {
		Animate();
		if(GetComponent<NetworkView>().isMine){
			if(GetComponent<Rigidbody2D>().velocity==Vector2.zero)
				Speed=0;
			else 
				Speed=1;
		}
	}
	void Start()
	{
		anim.enabled = true;
	}
	bool k = false;
	void LateUpdate()
	{
		if(k) return;
		if(!sr.gameObject.activeInHierarchy)
		{
			k = true;
			sr.gameObject.SetActive(true);
			sr.enabled = true;
		}else

		//if(!sr.sprite)
			sr.gameObject.SetActive(false);

	}
}
