﻿using UnityEngine;
using System.Collections;

public class NetworkMan : MonoBehaviour {
	public static NetworkMan shared;

	public static int connectPort=25001;
	public static string connectToIP="localhost";
	public static string CurrentScene="MainScene";
	public static string MapScene="MainScene";
	public static string MenuScene="MainMenuUI";
	private int lastLevelPrefix = 0;
	
	/*static NetworkMan()
	{
		GameObject obj = new GameObject ();
		obj.name = "NetworkMan";
		shared = obj.AddComponent<NetworkMan>();
		DontDestroyOnLoad (obj);
	}*/

	void Awake()
	{
		if (shared){ Destroy(gameObject); return;}
		shared = this;
		DontDestroyOnLoad (gameObject);
	}

	void Start()
	{
		Broadcast.resultReceived += OnGetResultBroadcast;
	}

	void OnFailedToConnectToMasterServer(NetworkConnectionError info) 
	{
		NotificationSystem.shared.ShowMessage ("<Error>MasterServer: " + info);
		Debug.Log("Could not connect to master server: " + info);
	}

	void OnMasterServerEvent(MasterServerEvent msEvent) 
	{
		NotificationSystem.shared.ShowMessage("<MasterServer> " + msEvent.ToString());
	}

	public static void StartServer()
	{
		Debug.Log ("start server");
		Network.InitializeServer(32, connectPort,!Network.HavePublicAddress());

		MasterServer.updateRate = -1;
		MasterServer.RegisterHost("BASEMiniBF", "Server #1", "game for all");

		//shared.OnServerInitialized ();
		//NetworkManager.autoServer = true;
		Broadcast.StartBroadcast ();
	}

	public static void Connect(string _connectToIP)
	{
		connectToIP = _connectToIP;
		Network.Connect(connectToIP, connectPort);
	}

	public static void ConnectToInternetGame(DoClick sender, string loadScene)
	{
		MasterServer.RequestHostList("BASEMiniBF");
		HostData[] data = MasterServer.PollHostList();
		if(data.Length > 0)
		{
			Network.Connect (data [0]);
			sender.SetScene(loadScene);
		}
	}

	public static void Disconnect()
	{
		Network.Disconnect(200);
	}

	void OnServerInitialized ()
	{
		//networkView.RPC("ReceiveNewPlayerInfo", RPCMode.AllBuffered, Nickname, MyInfo.id, MyInfo.ip, MyInfo.team);
		//EventSystem.GameStarted();
		//LoadLevel (MapScene);
	}
	
	public void LoadLevel(string lvl)
	{
		if(lvl == "")lvl = MapScene;
		//Network.RemoveRPCsInGroup(0);
		//Network.RemoveRPCsInGroup(1);
		GetComponent<NetworkView>().RPC("LoadLevelRPC", RPCMode.AllBuffered, lvl, lastLevelPrefix + 1);
		//EventSystem.GameStarted();
	}
	
	[RPC]
	void LoadLevelRPC(string lvl, int levelPrefix)
	{
		lastLevelPrefix = levelPrefix;
		
		Network.SetSendingEnabled(0, false); 
		
		// We need to stop receiving because first the level must be loaded first.
		// Once the level is loaded, rpc's and other state update attached to objects in the level are allowed to fire
		Network.isMessageQueueRunning = false;
		
		// All network views loaded from a level will get a prefix into their NetworkViewID.
		// This will prevent old updates from clients leaking into a newly created scene.
		Network.SetLevelPrefix(levelPrefix);
		//NotificationSystem.shared.ShowMessage ("HUYNYA");

		//MapScene = lvl;
		print (lvl);
		Application.LoadLevel(lvl);
	}
	
	void OnLevelWasLoaded(int level) 
	{
		Game.state = GameState.NonGame;

		if(Application.loadedLevelName != MapScene)return;

		// Allow receiving data again
		Network.isMessageQueueRunning = true;

		// Now the level has been loaded and we can start sending out data to clients
		Network.SetSendingEnabled(0, true);

		EventSystem.GameLoaded();
	}

	/*void  OnConnectedToServer ()
	{
		Application.LoadLevel ("MainScene");
	}*/
	void ExitToMenu()
	{
		Application.LoadLevel(MenuScene);
	}
	void  OnDisconnectedFromServer ( NetworkDisconnection info  )
	{
		ExitToMenu ();
		if (Network.isServer) 
		{
			MasterServer.UnregisterHost ();
			Broadcast.StopBroadcast ();
		}
	}
	
	void  OnFailedToConnect ( NetworkConnectionError error  )
	{
		Debug.Log ("OnFailedToConnect ");
		ExitToMenu ();
	}

	void OnGetResultBroadcast(string ip)
	{
		Debug.Log ("OnGetResultBroadcast "+ip);
		if(ip==Broadcast.NO_IP)
		{
			OnFailedToConnect(new NetworkConnectionError());
			return;
		}
		Connect(ip);
	}
}
