﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class GUIPlayerListRow : MonoBehaviour 
{
	public Text nameText;
	public Text scoreText;
	public Text killText;
	public Text deathsText;
	public int id;
	public int team { get { return NetworkManager.shared.PlayerList.Find(x=>x.id==id).team; } }
	public int score;

	void Set(string n, int s, int k, int d)
	{
		nameText.text = n;
		scoreText.text = s.ToString();
		killText.text = k.ToString();
		deathsText.text = d.ToString();
	}

	void Update()
	{
		int i = NetworkManager.shared.PlayerList.FindIndex(x => x.id == id);
		if(i < 0)
		{
			Destroy(gameObject);
			return;
		}

		NetworkPlayerInfo inf = NetworkManager.shared.PlayerList[i];
		Set(inf.nickname, inf.score, inf.kills, inf.deaths);
		score = inf.score;
	}
}
