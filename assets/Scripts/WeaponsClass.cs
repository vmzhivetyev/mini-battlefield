﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public enum WeaponName
{
	M4A1=0,
	AK74,
	RPK,
	SKS,
	DebugWeapon
}

[System.Serializable]
public struct Weapon
{
	public string name;
	[HideInInspector]
	public int currentBullets;
	public int maxBullets;
	public float fireRate;
	public float reloadTime;
	public float bulletSpeed;
	public float scatter;
	public Vector2 damageDistances;
	public Vector2 damageValues;
}

public class WeaponsClass : MonoBehaviour 
{
	public static WeaponsClass shared;

	public List<Weapon> Weapons;

	void Awake () 
	{
		if (shared) {
			Destroy (this);
			return;
		} 
		else 
			shared = this;
	}
}
