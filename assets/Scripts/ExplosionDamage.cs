﻿using UnityEngine;
using System.Collections;

public class ExplosionDamage : MonoBehaviour 
{
	public float damage;
	public float radius = 1.5f;
	public int damager = -1;

	void Start()
	{
		if(!Network.isServer)return;
		Invoke ("DoDamage", 0.1f);
	}

	void DoDamage()
	{
		Collider2D[] cols = Physics2D.OverlapCircleAll (transform.position, radius);
		foreach(Collider2D c in cols)
		{
			c.gameObject.SendMessage("SetDamagerID", damager, SendMessageOptions.DontRequireReceiver);
			c.gameObject.SendMessage("ApplyDamage",
			                         damage * (1 - Vector2.Distance(transform.position, c.gameObject.transform.position) / radius), SendMessageOptions.DontRequireReceiver); 
		}
	}
}
