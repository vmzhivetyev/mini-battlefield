﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class NotificationSystem : MonoBehaviour 
{
	public static NotificationSystem shared;
	private struct Message
	{
		public string text;
		public float deleteTime;
	}
	private List<Message> messages = new List<Message>();

	public float showTime = 2;


	public void ShowMessage(string msg)
	{
		messages.Add (new Message{ text = msg, deleteTime = Time.time + showTime });
	}

	void OnGUI()
	{
		foreach(Message m in messages)
		{
			GUILayout.BeginArea(new Rect(0, 0.7f * Screen.height, Screen.width, 0.3f * Screen.height));
			GUILayout.BeginVertical();
			GUILayout.BeginHorizontal();
			GUILayout.FlexibleSpace();
			GUILayout.Box(m.text, GUILayout.ExpandWidth(false), GUILayout.MinWidth(Screen.width / 5));
			GUILayout.FlexibleSpace();
			GUILayout.EndHorizontal();
			GUILayout.EndVertical();
			GUILayout.EndArea();
		}
	}

	void Awake()
	{
		if (shared)Destroy (this); 
		else shared = this;
	}

	void Update()
	{
		Message m;
		for(int i = 0; i < messages.Count; i++)
		{
			m = messages[i];
			if(m.deleteTime < Time.time)
			{
				messages.Remove(m);
			}
		}
	}
}
