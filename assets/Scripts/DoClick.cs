﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DoClick : MonoBehaviour 
{
	void Awake()
	{
		SetNick (PlayerPrefs.GetString("Nick",""));
	}

	void Update()
	{
		if (Application.loadedLevel==0 && Input.GetKey(KeyCode.Escape))
						Application.Quit ();
	}

	public void SetScene(string scene)
	{
		Application.LoadLevel (scene);
	}

	/*public void StartServer()
	{
		NetworkManager.autoServer = true;
		//NetworkMan.StartServer ();
		//Invoke ("StartServerInv",1);
	}*/

	public void StartServerInv()
	{
		NetworkMan.StartServer ();
	}

	public string ip="";

	public void SetIP(string ip)
	{
		this.ip = ip;
	}

	public void RequestHostlist()
	{
		MasterServer.RequestHostList("BASEMiniBF");
	}

	public void ConnectInternet(string loadScene)
	{
		NetworkMan.ConnectToInternetGame(this, loadScene);
	}

	public void ConnectServer()
	{
		if (ip == "") return;
		NetworkMan.Connect (ip);
	}

	public void Disconnect()
	{
		NetworkMan.Disconnect ();
	}

	public void Test()
	{
		int res = Broadcast.Test ();
		GameObject.Find ("Logo").GetComponent<Text> ().text = res.ToString();
	}

	public void SB()
	{
		Broadcast.StartBroadcast ();
		//Broadcast.StartSearching ();
	}

	public void StartSearch()
	{
		Broadcast.StartSearching (8);
	}

	public void SetNick(string name)
	{
		if (name == "") return;
		NetworkManager.shared.Nickname = name;
		PlayerPrefs.SetString ("Nick",name);
		var f = GameObject.Find ("Nick"); if(f) f.GetComponent<InputField>().text=name;
	}

	public static bool isShowStats=false;
	public void ShowStats()
	{
		isShowStats = !isShowStats;
	}
}
