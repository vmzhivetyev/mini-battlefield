﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class ResizeTile : MonoBehaviour {
	public float etalonSize = 0.5f;
	public bool canChange = true;
	public bool lockX;
	public bool lockY;
	void Start()
	{
		if (!Application.isEditor) enabled=false;
	}
	void Update () {
		if (canChange) {
			if(!lockX)GetComponent<Renderer>().sharedMaterial.SetFloat ("RepeatX", transform.lossyScale.x / etalonSize);
			else GetComponent<Renderer>().sharedMaterial.SetFloat ("RepeatX", 1);
			if(!lockY)GetComponent<Renderer>().sharedMaterial.SetFloat ("RepeatY", transform.lossyScale.y / etalonSize);
			else GetComponent<Renderer>().sharedMaterial.SetFloat ("RepeatY", 1);
		}else
		{
			Vector2 sc;
			sc.x = GetComponent<Renderer>().sharedMaterial.GetFloat("RepeatX")*etalonSize;
			sc.y = GetComponent<Renderer>().sharedMaterial.GetFloat("RepeatY")*etalonSize;
			Vector2 scl = transform.localScale;
			if(!lockX) scl.x = sc.x;//sc.x*sc.x/transform.lossyScale.x;
			if(!lockY) scl.y = sc.y;//sc.y*sc.y/transform.lossyScale.y;
			transform.localScale = scl;
		}
	}
}
