﻿using UnityEngine;
using System.Collections;

public class DestroyAfter : MonoBehaviour 
{
	public float time = 1.04f;
	public bool destroyAfterSound = false;

	void Start () 
	{
		Destroy (gameObject, destroyAfterSound ? Mathf.Max(GetComponent<AudioSource>().clip.length,time) : time);
	}
}
