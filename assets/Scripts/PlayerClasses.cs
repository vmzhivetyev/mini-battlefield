﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum ClassName
{
	Assault=0,
	Support,
	Recon,
	DebugClass
}

[System.Serializable]
public class PlayerClass
{
	public string name = "Class";
	public List<WeaponName> Weapons = new List<WeaponName>();
	public List<WeaponName> AvailableWeapons = new List<WeaponName>();
	public float movementSpeed = 4;
	public float maxHealthPoints = 100;
}

public class PlayerClasses : MonoBehaviour 
{
	public static PlayerClasses shared;
	public List<PlayerClass> classes = new List<PlayerClass>();

	void Awake () 
	{
		if (shared) {
			Destroy (this);
			return;
		} 
		else 
			shared = this;
	}
}
