﻿using UnityEngine;
using System.Collections;
using System;

public class EventSystem : MonoBehaviour 
{
	public static EventSystem shared;

	public static event Action OnGameStarted=delegate{};
	public static event Action OnGameEnded=delegate{};
	public static event Action OnGameLoaded=delegate{};
	public static event Action OnGamePreStart=delegate{};

	public void Awake()
	{
		if (shared){ Destroy(gameObject); return;}
		shared = this;
		DontDestroyOnLoad (gameObject);
	}

	public static void GameStarted()
	{
		shared.GetComponent<NetworkView>().RPC ("GameStartedRPC", RPCMode.All);
	}
	public static void GameEnded()
	{
		shared.GetComponent<NetworkView>().RPC ("GameEndedRPC", RPCMode.All);
	}
	public static void GameLoaded ()
	{
		shared.GameLoadedRPC();
		//shared.networkView.RPC ("GameLoadedRPC", RPCMode.All);
	}
	public static void GamePreStart ()
	{
		shared.GetComponent<NetworkView>().RPC ("GamePreStartRPC", RPCMode.All);
	}

	[RPC]
	void GameStartedRPC()
	{
		OnGameStarted();
	}
	[RPC]
	void GameEndedRPC()
	{
		OnGameEnded();
	}
	[RPC]
	void GameLoadedRPC()
	{
		OnGameLoaded();
	}
	[RPC]
	void GamePreStartRPC()
	{
		OnGamePreStart ();
	}
}
