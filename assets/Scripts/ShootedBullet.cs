﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class ShootedBullet : MonoBehaviour 
{
	/*
	public float damage;
	public float hitSpeedMultiplier = 0.5f;
	public float hitDamageMultiplier = 0.5f;
	public int maxHits = 2;
	public int shooterId;
	public float force;
	public Vector3 lastPos;
	public LayerMask layerMask;
	public string[] shootableTags;//простреливаются

	private Vector3 position;
	private Vector3 direction;
	private RaycastHit hit;

	public bool wasHit = false;

	void Start() 
	{
		lastPos = transform.position;
	}

	void Update() 
	{
		if(wasHit) return;

		position = transform.position;
		direction = position - lastPos;

		if(position.y < -500)
			Destroy(gameObject);
		
		if(Physics.Raycast(lastPos, direction, out hit, Vector3.Distance(position, lastPos), layerMask.value))
		{
			Vector3 contact = hit.point;


			//Debug.Log(hit.transform.name);
			bool shootable = shootableTags.Contains(hit.transform.tag) && maxHits > 1;
			Debug.Log("shootable "+shootable);
			if(shootable)
			{
				transform.position = hit.point + rigidbody.velocity.normalized * 0.1f;
				if(hit.transform.tag != "ShootableAbsolute")
				{
					rigidbody.velocity *= hitSpeedMultiplier;
					maxHits--;
				}
			}
			else
			{
				transform.position = contact;
				rigidbody.velocity = Vector3.zero;
				rigidbody.useGravity = false;
				wasHit = true;
			}

			Quaternion hitRotation = Quaternion.FromToRotation(Vector3.up, hit.normal);
			
			if(hit.rigidbody)
				hit.rigidbody.AddForceAtPosition(force * direction.normalized, hit.point);
			
			string pTag = hit.transform.tag;
			if(UserInterface.shared.weaponManager.GetIsAliveTag(hit.transform.tag))
			{
				pTag = "Alive";
				SendMessageUpwards("DrawCrosshair", SendMessageOptions.DontRequireReceiver);
			}
			else if(!UserInterface.shared.weaponManager.Particles.Any(x => x.tag == pTag))
			{
				pTag = "Untagged";
			}
			
			GameObject particlePref = pTag == "Alive" ? UserInterface.shared.weaponManager.particleBlood : UserInterface.shared.weaponManager.Particles.FirstOrDefault(x=>x.tag == pTag).particlePrefab;
			GameObject bulletHole = Network.Instantiate(particlePref, contact, hitRotation, 1) as GameObject;
			bulletHole.transform.parent = hit.transform;
			
			hit.collider.SendMessageUpwards("ApplyDamageFromPlayer", MainMenuScript.shared.PlayerID, SendMessageOptions.DontRequireReceiver);
			hit.collider.SendMessage	   ("ApplyDamageFromPlayer", MainMenuScript.shared.PlayerID, SendMessageOptions.DontRequireReceiver);
			
			hit.collider.SendMessageUpwards("ApplyDamage", 	  damage, SendMessageOptions.DontRequireReceiver);
			hit.collider.SendMessage	   ("ApplyDamageHead", damage, SendMessageOptions.DontRequireReceiver);

			if(shootable)
			{
				if(hit.transform.tag != "ShootableAbsolute")
					damage *= hitDamageMultiplier;
			}
			else
			{
				StartCoroutine("ApplyForceToHit");
			}
		}
		lastPos = transform.position;
	}
	
	IEnumerator ApplyForceToHit()
	{
		yield return new WaitForSeconds(0.01f);
		if(Physics.Raycast(position, direction, out hit, Vector3.Distance(position, lastPos), layerMask.value))
		{
			if(hit.rigidbody)
				hit.rigidbody.AddForceAtPosition(force * direction.normalized, hit.point);
		}
		Destroy(gameObject);
	}*/
}
