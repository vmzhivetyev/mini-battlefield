﻿using UnityEngine;
using System.Collections;

public class CarPhysics : MonoBehaviour {

	public float maxMotorForce=20;
	public float maxTorque=20;
	private float maxVelocityChange = 0.2f;
	public Vector2 speedLimits = new Vector2(-5,10);
	public Vector2 enginePitch = new Vector2 (0.9f, 1.5f);
	float x,y;
	bool back = false;
	public bool controllable = false;
	[HideInInspector]
	public float linearDrag = 5;
	public float sideStabilizationAcc = 10;
	public float sideVelocityMultiplier = 0.2f;
	public float angularAcceleration = 10;

	private float prevVel = 0;

	public void JoystickTouchStart(MovingJoystick move)
	{
		if(move.joystickName != "MovingJoystick")return;
		back = true;
	}
	public void JoystickTouchEnd(MovingJoystick move)
	{
		if(move.joystickName != "MovingJoystick")return;
		back = false;
	}
	public void JoystickMove( MovingJoystick move)
	{
		if(move.joystickName != "MovingJoystick")return;
		x = Mathf.Clamp(move.joystickAxis.x * 2, -1, 1);
		y = Mathf.Clamp(move.joystickAxis.y * 2, -1, 1);
		back=false;
	}
	
	public void JoystickMoveEnd( MovingJoystick move)
	{
		if(move.joystickName != "MovingJoystick")return;
		x = 0; y = 0;
	}

	public void RegisterEvents () 
	{
		controllable = true;
		x = 0; y = 0;
		EasyJoystick.On_JoystickMove += JoystickMove;
		EasyJoystick.On_JoystickMoveEnd += JoystickMoveEnd;
		EasyJoystick.On_JoystickTouchStart += JoystickTouchStart;
		EasyJoystick.On_JoystickTouchUp += JoystickTouchEnd;
		GetComponent<Rigidbody2D>().isKinematic = false;
		NetworkManager.shared.rightControls.SetActive(false);
		NetworkManager.shared.leftControls.SetActive(true);
	}

	public void UnRegisterEvents()
	{
		if(!controllable)return;
		controllable = false;
		if(Network.isClient)
		GetComponent<Rigidbody2D>().isKinematic = true;
		//rigidbody2D.velocity = Vector2.zero;
		//rigidbody2D.angularVelocity = 0;
		EasyJoystick.On_JoystickMove -= JoystickMove;
		EasyJoystick.On_JoystickMoveEnd -= JoystickMoveEnd;
		EasyJoystick.On_JoystickTouchStart -= JoystickTouchStart;
		EasyJoystick.On_JoystickTouchUp -= JoystickTouchEnd;

		x = 0; y = 0;
		GetComponent<Rigidbody2D>().drag = linearDrag;

		if(NetworkManager.shared.leftControls)
			NetworkManager.shared.leftControls.SetActive(true);
		if(NetworkManager.shared.rightControls)
			NetworkManager.shared.rightControls.SetActive(true);
	}

	void OnDestroy()
	{
		UnRegisterEvents();
	}

	void Start()
	{
		linearDrag = GetComponent<Rigidbody2D>().drag;
		GetComponent<Rigidbody2D>().centerOfMass = (GetComponent<Collider2D>() as BoxCollider2D).offset;
	}

	public float acc;
	public float p;

	void Update()
	{
		//PlaySound (Mathf.Clamp (rigidbody2D.velocity.magnitude / speedLimits.y, enginePitch.x, enginePitch.y));
		if(controllable || (Network.isServer && !GetComponent<CarNetwork>().hasDriver))
		{
			//acc = Mathf.Clamp(rigidbody2D.velocity.magnitude - prevVel, 0, 100);
			//p = acc / maxMotorForce; // from 0 to 1
			p = GetComponent<Rigidbody2D>().velocity.magnitude / speedLimits.y;

			if(GetComponent<Car>().healthPoints > 0)
				GetComponent<NetworkView>().RPC("SetEnginePitch", RPCMode.All, p * (enginePitch.y - enginePitch.x) + enginePitch.x);
			prevVel = GetComponent<Rigidbody2D>().velocity.magnitude;
		}
	}

	void FixedUpdate () 
	{
		if(!controllable)return;
		float _x = 0,_y = 0;
		if(Input.GetAxis ("Horizontal") != 0 || Input.GetAxis ("Vertical") != 0)
			NetworkManager.shared.ezjoystick.enabled = false;
		if(!NetworkManager.shared.ezjoystick.enabled)
		{
			//print ("abc");
			x = Input.GetAxis ("Horizontal");
			y = Input.GetAxis ("Vertical");
			_x = x; _y = y;
		}
		else if(new Vector2(x,y) != Vector2.zero)
		{
			Vector2 joy = new Vector2(x,y);
			Vector2 up = transform.up;
			float cross = Vector3.Cross((Vector3)joy, (Vector3)up).z;
			//_x = cross;
			//_y = new Vector2(_x, 1).normalized.y;
			_x = new Vector2(cross, 1).normalized.x;
			_y = new Vector2(x,y).magnitude;
			float dirAngle = Vector2.Angle(new Vector2(x,y), transform.up);
			if(dirAngle > 90)// && dirAngle < 150)
			{
				_y = 0.8f;
				_x = _x < 0 ? -1 : 1;
			}
			/*if(dirAngle >= 150)
			{
				_y *= -1;
			}*/
		}
		if(back){ _x=0;_y=-1;}
		var dir = new Vector2 (_x, _y);

		float angle = Vector2.Angle(GetComponent<Rigidbody2D>().velocity, transform.up);

		float forwardVelAbs = Mathf.Cos (angle * Mathf.Deg2Rad) * GetComponent<Rigidbody2D>().velocity.magnitude;
		if(Mathf.Abs(forwardVelAbs) < speedLimits.y * Mathf.Abs(_y))
			GetComponent<Rigidbody2D>().AddForce(transform.up*maxMotorForce*_y*GetComponent<Rigidbody2D>().mass);
		if(forwardVelAbs < 0) _x = -_x;

		Vector2 forwardVel = transform.up * Mathf.Clamp(forwardVelAbs, speedLimits.x, speedLimits.y);

		float cross1 = Vector3.Cross(transform.up, (Vector3)GetComponent<Rigidbody2D>().velocity).z;

		float sideVelAbs = Mathf.Sin (angle * Mathf.Deg2Rad) * GetComponent<Rigidbody2D>().velocity.magnitude;
		sideVelAbs = Mathf.Clamp(sideVelAbs - sideStabilizationAcc * Time.fixedDeltaTime, 0, sideVelAbs) * (cross1 < 0 ? 1 : -1);
		Vector2 sideVelocity = transform.right * sideVelAbs;

		var targetVelocity = forwardVel + sideVelocity*sideVelocityMultiplier;

		var velocity = GetComponent<Rigidbody2D>().velocity;
		var velocityChange = (targetVelocity - velocity);
		velocityChange.x = Mathf.Clamp(velocityChange.x, -maxVelocityChange, maxVelocityChange);
		velocityChange.y = Mathf.Clamp(velocityChange.y, -maxVelocityChange, maxVelocityChange);
		GetComponent<Rigidbody2D>().AddForce(velocityChange*GetComponent<Rigidbody2D>().mass/Time.fixedDeltaTime);

		GetComponent<Rigidbody2D>().angularVelocity = Mathf.Lerp(GetComponent<Rigidbody2D>().angularVelocity, -_x*maxTorque*forwardVel.magnitude, Time.fixedDeltaTime*angularAcceleration);
		//rigidbody2D.drag = Mathf.Lerp(rigidbody2D.drag, dir.y == 0 ? linearDrag : 0, 2*Time.fixedDeltaTime);
	}

	[RPC]
	void SetEnginePitch(float p)
	{
		GetComponent<AudioSource>().pitch = Mathf.Lerp(GetComponent<AudioSource>().pitch, p, 0.2f);
	}
}
