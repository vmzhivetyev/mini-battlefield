﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class ShootingController : MonoBehaviour 
{
	public GameObject bulletPrefab;
	public Transform shotPos;
	public bool includeSelfVelocity = false;

	#region
	[HideInInspector]
	public bool canAttack = true;
	public float lastFireFrame;
	public float _lastFireFrame;
	private float reloadingTimeLeft = 0;

	public int currentWeaponIndex = 0;
	[HideInInspector]
	public List<WeaponName> startWeapons;
	[HideInInspector]
	public Weapon[] weapons;
	public Weapon currentWeapon { get { return weapons[currentWeaponIndex]; } set { weapons[currentWeaponIndex] = value; } }
	public int currentBullets
	{
		get { return currentWeapon.currentBullets; }
		set { Weapon w = currentWeapon; w.currentBullets = value; currentWeapon = w; }
	}
	public float fireRate    { get { return currentWeapon.fireRate;    } }
	public int maxBullets    { get { return currentWeapon.maxBullets;  } }
	public float reloadTime  { get { return currentWeapon.reloadTime;  } }
	public float bulletSpeed { get { return currentWeapon.bulletSpeed; } }
	public bool isRotating  { get { return (x != 0 || y != 0) || (_lastFireFrame + 1 > Time.time); } }
	private bool canReload  { get { return reloadingTimeLeft <= 0 && currentBullets < maxBullets; } }
	public PlayerSoundSystem soundSystem { get { return GetComponent<PlayerSoundSystem>(); } }
	#endregion

	private bool justShoot = false;
	private float x,y;
	
	void JoystickTouchStart(MovingJoystick move)
	{
		if(move.joystickName != "ShootingJoystick")return;
		//x = move.joystickAxis.x;
		//y = move.joystickAxis.y;
		//Debug.Log(x + " " + y);
		justShoot = true;
	}

	void JoystickTouchEnd(MovingJoystick move)
	{
		if(move.joystickName != "ShootingJoystick")return;
		justShoot = false;
	}

	void JoystickMove( MovingJoystick move)
	{
		if(move.joystickName != "ShootingJoystick")return;
		x = move.joystickAxis.x;
		y = move.joystickAxis.y;

		justShoot=false;//x==0 && y==0;//false;
	}

	public void JoystickCancel()
	{
		justShoot = false;
		x = 0; y = 0;
	}
	
	void JoystickMoveEnd( MovingJoystick move)
	{
		if(move.joystickName != "ShootingJoystick")return;
		x = 0; y = 0;
		//justShoot=true;
	}

	void BtnPressed(string btnName)
	{
		if(btnName == "ReloadButton" && canReload)
		{
			StartReloading();
		}
	}

	void Start () 
	{
		if (!GetComponent<NetworkView>().isMine)
		{
			enabled = false;
			return;
		}

		currentWeaponIndex = 0;

		weapons = PlayerClasses.shared.classes [(int)(GetComponent<Player> ().currentClass)].Weapons.Select (x => WeaponsClass.shared.Weapons [(int)x]).ToArray ();//new Weapon[startWeapons.Count];
		//startWeapons = PlayerClasses.shared.classes[(int)(GetComponent<Player>().currentClass)].Weapons;
		//weapons = new Weapon[startWeapons.Count];
		//for(int i = 0; i < startWeapons.Count; i++)
		//	weapons[i] = WeaponsClass.shared.Weapons[(int)startWeapons[i]];
		currentBullets = maxBullets;

		if(!GetComponent<Player>().isBot)
		{
			EasyButton.On_ButtonDown += BtnPressed;
			EasyJoystick.On_JoystickMove += JoystickMove;
			EasyJoystick.On_JoystickMoveEnd += JoystickMoveEnd;
			EasyJoystick.On_JoystickTouchStart += JoystickTouchStart;
			EasyJoystick.On_JoystickTouchUp += JoystickTouchEnd;
		}
	}

	void StartReloading()
	{
		reloadingTimeLeft = reloadTime;
		GetComponent<NetworkView>().RPC("ReloadSoundRPC", RPCMode.All);
	}

	void Update () 
	{
		if(GetComponent<CarEnter>().inCar && GetComponent<CarEnter>().cn.IsDriver(gameObject) && (x != 0 || y != 0 || justShoot))
			JoystickCancel();

		if(reloadingTimeLeft > 0)
		{
			reloadingTimeLeft -= Time.deltaTime;
			if(reloadingTimeLeft <= 0)
				currentBullets = maxBullets;
		}

		var dir = new Vector2 (x, y);
		if (dir != Vector2.zero) 
		{
			transform.LookAt (transform.position + new Vector3 (0, 0, 1), new Vector3 (x, y, 0));
			ShootNow();
			return;
		}
		else if(justShoot || (GetComponent<Player>().isBot && GetComponent<Bot>().attackingNow))
		{
			ShootNow();
			return;
		}
	}

	void OnDrawGizmos()
	{
		Gizmos.DrawWireSphere (transform.position, currentWeapon.damageDistances.x);
		Gizmos.DrawWireSphere (transform.position, currentWeapon.damageDistances.y);
	}

	void ShootNow()
	{
		if(lastFireFrame + fireRate <= Time.time && canAttack)
		{
			OneShot();
			lastFireFrame = Time.time;
			if(!justShoot) _lastFireFrame=lastFireFrame;
		}
	}
	
	void OneShot()
	{
		if(currentBullets <= 0 || reloadingTimeLeft > 0)
		{
			lastFireFrame = 0;
			if(reloadingTimeLeft <= 0)
				currentBullets = maxBullets;
			return;
		}
		else
		{
			currentBullets--;
			if(currentBullets <= 0)
			{
				StartReloading();
			}
		}
		Vector2 vel = (Vector2)transform.up * bulletSpeed + (includeSelfVelocity ? (GetComponent<CarEnter>().cn ? (Vector2)(GetComponent<CarEnter>().cn.carSpeed) : GetComponent<Rigidbody2D>().velocity) : Vector2.zero);
		vel = vel.normalized * Mathf.Clamp(vel.magnitude, bulletSpeed, float.MaxValue);

		vel = (Vector2)GameSettings.RotateAroundAxis (vel, Random.Range (-currentWeapon.scatter, currentWeapon.scatter), Vector3.forward);

		if(Network.isServer)
			OneShotRPC(GetComponent<Player>().id, vel.x, vel.y, currentWeapon.damageDistances, currentWeapon.damageValues);
		else
			GetComponent<NetworkView>().RPC("OneShotRPC", RPCMode.Server, GetComponent<Player>().id, vel.x, vel.y, (Vector3)currentWeapon.damageDistances, (Vector3)currentWeapon.damageValues);
		GetComponent<NetworkView>().RPC ("ShotSoundRPC", RPCMode.All);
	}

	[RPC]
	void OneShotRPC(int shooterId, float x, float y, Vector3 distances, Vector3 damage)
	{
		//Debug.Log("instantiate here");
		GameObject go = Network.Instantiate (bulletPrefab, shotPos ? shotPos.position : transform.position, transform.rotation, 0) as GameObject;
		go.GetComponent<NetworkView>().RPC("SyncVelocity", RPCMode.All, shooterId, !GetComponent<Player>().isBot, x, y, (Vector3)distances, (Vector3)damage);
	}

	void OnDestroy()
	{
		if(!GetComponent<Player>().isBot)
		{
			EasyButton.On_ButtonDown -= BtnPressed;
			EasyJoystick.On_JoystickMove -= JoystickMove;
			EasyJoystick.On_JoystickMoveEnd -= JoystickMoveEnd;
			EasyJoystick.On_JoystickTouchStart -= JoystickTouchStart;
			EasyJoystick.On_JoystickTouchUp -= JoystickTouchEnd;
		}
	}
}
