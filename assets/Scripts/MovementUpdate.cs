using UnityEngine;
using System.Collections;

public class MovementUpdate : MonoBehaviour 
{
	public GameObject go;//whats synching

	[HideInInspector]
	public Vector3 newPosition;
	[HideInInspector]
	public Quaternion newRotation;
	public Quaternion newRotation2;

	//public float Vel = 8;
	private float maximumLerpDistance = 2;
	
	private float movementDamping = 10;
	private float rotationDamping = 15;

	private bool autoObserveNetworkView = true;

	void OnSerializeNetworkView(BitStream stream, NetworkMessageInfo info) 
	{
		if(stream.isWriting)
		{
			if(enabled)
			{
			Vector3 pos = go.transform.position + (go.tag!="Carcar"?((Vector3)GetComponent<Rigidbody2D>().velocity * 0.075f):Vector3.zero);
			Quaternion rot = go.transform.rotation;
			stream.Serialize(ref pos);
			stream.Serialize(ref rot);
			}
			else
			{
				Quaternion q = transform.rotation;
				stream.Serialize(ref q);
			}
		}
		else
		{
			if(enabled)
			{
			stream.Serialize(ref newPosition);
			stream.Serialize(ref newRotation);
			}
			else
			{
				stream.Serialize(ref newRotation2);
			}
		}
	}

	void Awake()
	{
		if(!go)
			go = gameObject;
	}

	void Start()
	{
		newPosition = go.transform.position;
		newRotation = go.transform.rotation;
		newRotation2 = newRotation;

		if(autoObserveNetworkView)
		{
			GetComponent<NetworkView>().observed = this;
			GetComponent<NetworkView>().stateSynchronization = NetworkStateSynchronization.Unreliable;
		}
	}

	void Update()
	{
		if(GetComponent<NetworkView>().isMine)
			return;

		if(Vector3.Distance(go.transform.position, newPosition) >= (go.tag == "Car" ? 8 : maximumLerpDistance))
		{
			go.transform.position = newPosition;
			go.transform.rotation = newRotation;
			if(go != gameObject)
				transform.rotation = newRotation2;
		}
		go.transform.position = Vector3.Lerp(go.transform.position, newPosition, Time.deltaTime * (go.tag == "Car" ? 6 : movementDamping));
		go.transform.rotation = Quaternion.Lerp(go.transform.rotation, newRotation, Time.deltaTime * rotationDamping);
	}
}