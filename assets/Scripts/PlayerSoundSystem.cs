﻿using UnityEngine;
using System.Collections;

public class PlayerSoundSystem : MonoBehaviour
{
	public AudioClip shotSound;
	public AudioClip reloadSound;
	public AudioClip stepSound;
	public AudioClip damageSound;
	public AudioClip fuckYouSound;
	public AudioClip spawnSound;
	
	public float stepsAudioSpeed = 0.4f;

	public AudioSource stepsAudioSource;

	private float stepAudioTimer = 0.0f;
	private float moveSpeed { get { return PlayerClasses.shared.classes[(int)GetComponent<Player>().currentClass].movementSpeed; } }
	private bool needFootsteps { get { return GetComponent<Rigidbody2D>().velocity.magnitude > moveSpeed*0.7f; } }

	void Start () 
	{
		//if (networkView.isMine)
		//				gameObject.AddComponent<AudioListener> ();
		if(GetComponent<NetworkView>().isMine)
		{
			GetComponent<NetworkView>().RPC("SpawnSoundRPC", RPCMode.All);
		}
	}

	void Update () 
	{
		PlayFootsteps();
	}

	void PlayFootsteps()
	{
		if(needFootsteps)
		{
			if (stepAudioTimer >= stepsAudioSpeed)
			{
				GetComponent<NetworkView>().RPC("Step", RPCMode.All);
				stepAudioTimer = 0.0f;
			}
		}
		stepAudioTimer += Time.deltaTime;
	}
	
	[RPC]
	void SpawnSoundRPC()
	{
		if(spawnSound)
			GetComponent<AudioSource>().PlayOneShot(spawnSound);
	}

	[RPC]
	void ReloadSoundRPC()
	{
		if(reloadSound)
			GetComponent<AudioSource>().PlayOneShot(reloadSound);
	}

	[RPC]
	void FKYou()
	{
		if(fuckYouSound)
			GetComponent<AudioSource>().PlayOneShot(fuckYouSound);
	}

	[RPC]
	void Step()
	{
		stepsAudioSource.PlayOneShot(stepSound);
	}

	[RPC]
	void ShotSoundRPC()
	{
		GetComponent<AudioSource>().PlayOneShot(shotSound);
	}

	public void GetDamage()
	{
		GetComponent<AudioSource>().PlayOneShot (damageSound);
	}
}
