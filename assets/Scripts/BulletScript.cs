﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class BulletScript : MonoBehaviour 
{
	public SpriteRenderer sprite;

	public Vector2 distances = new Vector2(10,10);
	public Vector2 damage = new Vector2(10,10);
	private Vector3 instPos;
	//public float maxDist = 10;
	public int shooterId = -1;
	public bool isPlayer = true; // who shooted (player/bot)
	
	public Vector3 lastPos;
	public LayerMask layerMask;
	public LayerMask botLayerMask;

	private Vector3 position;
	private Vector3 direction;
	private RaycastHit2D hit;
	private float initializationTime;
	private float lifeTime = 5;

	void Start () 
	{
		//Destroy (gameObject, maxDist / rigidbody2D.velocity.magnitude);
		//enabled = false;
	}

	[RPC]
	void SyncVelocity(int _shooterId, bool playerShooted, float x, float y, Vector3 _distances, Vector3 _damage)
	{
		GetComponent<Rigidbody2D>().velocity = new Vector2(x,y);
		isPlayer = playerShooted;
		shooterId = _shooterId;
		distances = _distances;
		damage = _damage;

		if(NetworkManager.shared.PlayerList.Any(s => s.id == shooterId && s.gameObject))
		{
			ShootingController sc = NetworkManager.shared.PlayerList.Find(a => a.id == shooterId).gameObject.GetComponent<ShootingController>();
			transform.position = sc.shotPos ? sc.shotPos.transform.position : sc.transform.position;
			instPos = transform.position;
		}

		if(!isPlayer)
		{
			layerMask = botLayerMask;
		}

		if(isPlayer && shooterId == NetworkManager.MyInfo.id)
			enabled = true;
		if(!isPlayer && Network.isServer)
			enabled = true;

		if(enabled)
		{
			lastPos = transform.position;
			initializationTime = Time.time;
		}
	}

	void Update()
	{
		/*
		if(isPlayer)
			if(shooterId != NetworkManager.MyInfo.id)return;
		else 
			if(!Network.isServer)return;*/

		//if(Network.isServer)
			if(initializationTime + lifeTime < Time.time)
				DestroyMyself();

		position = transform.position;
		direction = position - lastPos;
		lastPos = transform.position;

		hit = (Physics2D.Raycast (lastPos, direction, Vector2.Distance (position, lastPos), layerMask.value));
		if (hit)
		{
			if(hit.transform.tag == "Player" || hit.transform.tag == "Bot")
			{
				//create particles
				//Debug.Log(hit.transform.gameObject.GetComponent<Player>().id + " " + shooterId);
				if(hit.transform.gameObject.GetComponent<Player>().id == shooterId)
					return;
			}
			
			//enabled = false;
			Vector2 contact = hit.point;
			transform.position = contact;
			GetComponent<Rigidbody2D>().velocity = Vector2.zero;
			if(sprite) sprite.enabled = false;

			{
				float traveledDist = Vector3.Distance(instPos, contact);
				float d = traveledDist <= distances.x ? damage.y : (traveledDist < distances.y ? ((1-(traveledDist - distances.x)/(distances.y - distances.x))*(damage.y - damage.x) + damage.x) : damage.x);
				if(hit.transform.tag == "Player")// && !(hit.transform.gameObject.GetComponent<Player>().team == NetworkManager.MyInfo.team))
				{	
					hit.collider.SendMessage("SetDamagerID", shooterId, SendMessageOptions.DontRequireReceiver);
					hit.collider.SendMessage("ApplyDamage" ,         d, SendMessageOptions.DontRequireReceiver);
				}
				else if(hit.transform.tag != "Player")
				{
					hit.collider.SendMessage("SetDamagerID", shooterId, SendMessageOptions.DontRequireReceiver);
					hit.collider.SendMessage("ApplyDamage" ,         d, SendMessageOptions.DontRequireReceiver);
				}
				//Debug.Log("Distance "+traveledDist+"; damage " + d);
			}

			DestroyMyself();
		}
	}

	void DestroyMyself()
	{
		enabled = false;
		//Network.RemoveRPCs(networkView.viewID);
		//Network.Destroy (gameObject);
		if(Network.isServer)
			DestroyMyselfRPC();
		else
			GetComponent<NetworkView>().RPC("DestroyMyselfRPC", RPCMode.Server);
	}

	[RPC]
	void DestroyMyselfRPC()
	{
		Network.RemoveRPCs(GetComponent<NetworkView>().viewID);
		Network.Destroy (GetComponent<NetworkView>().viewID);
	}
}
