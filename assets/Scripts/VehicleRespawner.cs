﻿using UnityEngine;
using System.Collections;

public enum VehicleType
{
	Car = 0,
	Tank,
	Heli
}

public class VehicleRespawner : MonoBehaviour 
{
	public int ownerTeam = 0;
	public GameObject vehiclePref;
	public VehicleType vehicleType;
	//[HideInInspector]
	public GameObject lastSpawnedObject;

	private float respawnTime { get { return GameSettings.vehicleRespawnTime; } }
	private bool spawnInvoked = true;

	void Awake ()
	{
		if(!vehiclePref)
		{
			enabled = false;
			return;
		}
		EventSystem.OnGameStarted += OnGameStarted;
		EventSystem.OnGamePreStart += OnGamePreStart;
		EventSystem.OnGameEnded += OnGameEnded;
	}
	void OnDestroy()
	{
		EventSystem.OnGameStarted -= OnGameStarted;
		EventSystem.OnGamePreStart -= OnGamePreStart;
		EventSystem.OnGameEnded -= OnGameEnded;
	}
	void OnGameEnded()
	{
		//DestroyVehicle();
	}
	void OnGamePreStart()
	{
		if (!Network.isServer) return;
		//print ("DestroyCar!");
		spawnInvoked = true;
		DestroyVehicle();
	}
	void OnGameStarted()
	{
		if (!Network.isServer) return;
		//print ("SpawnCar!");
		//Spawn();
		Spawn();
	}

	void Update () 
	{
		if(!Network.isServer)return;
		if(!spawnInvoked && !lastSpawnedObject)
		{
			print (@"Invoke Spawn Car");
			spawnInvoked = true;
			Invoke("Spawn", respawnTime);
		}
	}

	void DestroyVehicle()
	{
		if(lastSpawnedObject)
		{
			//Network.RemoveRPCs(lastSpawnedObject.networkView.viewID);
			//Network.Destroy(lastSpawnedObject);
			Car c = lastSpawnedObject.GetComponent<Car>();
			c.SetDamagerID(-10);
			c.ApplyDamage(c.healthPoints);
		}
	}

	void Spawn()
	{
		DestroyVehicle ();
		spawnInvoked = false;
		lastSpawnedObject = Network.Instantiate(vehiclePref, transform.position, transform.rotation, 1) as GameObject;
		var car = lastSpawnedObject.GetComponent<Car> ();
		if(car) car.ownerTeam = ownerTeam;
	}
}
