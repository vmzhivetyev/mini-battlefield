﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour 
{
	public Transform goal;
	public Transform audioListener;
	public bool dontGoAwayWhenDead = true;

	public EdgeCollider2D screenEdge;
	public Vector2 xBorders;
	public Vector2 yBorders;
	public Vector2 xCamBorders;
	public Vector2 yCamBorders;
	public bool justFollow = false;
	public float speed = 10;

	private float aspectRatio;

	void CalcCamValues()
	{
		aspectRatio = (Screen.width / (float)Screen.height);
		if(screenEdge)
		{
			xBorders.x = screenEdge.bounds.center.x - screenEdge.bounds.extents.x;
			xBorders.y = screenEdge.bounds.center.x + screenEdge.bounds.extents.x;
			yBorders.x = screenEdge.bounds.center.y - screenEdge.bounds.extents.y;
			yBorders.y = screenEdge.bounds.center.y + screenEdge.bounds.extents.y;
			xCamBorders.x = xBorders.x + GetComponent<Camera>().orthographicSize * aspectRatio;
			xCamBorders.y = xBorders.y - GetComponent<Camera>().orthographicSize * aspectRatio;
			yCamBorders.x = yBorders.x + GetComponent<Camera>().orthographicSize;
			yCamBorders.y = yBorders.y - GetComponent<Camera>().orthographicSize;
		}
	}

	void CreateAudioListener()
	{
		if(GetComponent<AudioListener> ())
			GetComponent<AudioListener> ().enabled = false;

		if(FindObjectOfType<AudioListener>() == null)
		{
			GameObject g = new GameObject();
			g.name = "_AudioListener";
			g.AddComponent<AudioListener>();
			audioListener = g.transform;
		}
		else
		{
			audioListener =  FindObjectOfType<AudioListener>().transform;
		}
	}

	void Start () 
	{
		CreateAudioListener ();
		CalcCamValues ();
	}

	void Update()
	{
		xCamBorders.x = xBorders.x + GetComponent<Camera>().orthographicSize * aspectRatio;
		xCamBorders.y = xBorders.y - GetComponent<Camera>().orthographicSize * aspectRatio;
		yCamBorders.x = yBorders.x + GetComponent<Camera>().orthographicSize;
		yCamBorders.y = yBorders.y - GetComponent<Camera>().orthographicSize;
		//Debug.DrawLine(new Vector3(xCamBorders.x, yCamBorders.x, 0), new Vector3(xCamBorders.x, yCamBorders.y, 0), Color.green);
	}

	void LateUpdate () 
	{
		if(goal)
		{
			Vector3 targetPos = new Vector3(goal.position.x, goal.position.y, transform.position.z);
			if(!justFollow)targetPos = new Vector3( Mathf.Clamp(targetPos.x, xCamBorders.x, xCamBorders.y), Mathf.Clamp(targetPos.y, yCamBorders.x, yCamBorders.y), targetPos.z);
			transform.position = Vector3.Lerp(transform.position, targetPos, Time.smoothDeltaTime*speed);

			MoveAudioListener();
		}
	}

	void MoveAudioListener()
	{
		if(!audioListener)return;

		audioListener.position = goal ? goal.position : transform.position;
	}
}
