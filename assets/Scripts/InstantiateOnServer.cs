﻿using UnityEngine;
using System.Collections;

public class InstantiateOnServer : MonoBehaviour 
{
	public GameObject[] objects;
	public Transform[] positions;

	void OnServerInitialized()
	{
		for(int i = 0; i < objects.Length; i++)
			Network.Instantiate(objects[i], positions[Mathf.Clamp(i, 0, positions.Length)].position, objects[i].transform.rotation, 0);
	}

	void Update()
	{
		if(Input.GetKeyDown(KeyCode.E) && SpawnScript.shared.myPlayer)
		{
			GameObject g = GameObject.FindGameObjectWithTag("Car");
			g.GetComponent<CarNetwork>().Enter(SpawnScript.shared.myPlayer);
		}

		if(Input.GetKeyDown(KeyCode.R) && SpawnScript.shared.myPlayer)
		{
			GameObject g = GameObject.FindGameObjectWithTag("Car");
			g.GetComponent<CarNetwork>().Exit(SpawnScript.shared.myPlayer);
		}
	}
}
