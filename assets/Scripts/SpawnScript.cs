﻿// Converted from UnityScript to C# at http://www.M2H.nl/files/js_to_c.php - by Mike Hergaarden
// Do test the code! You usually need to change a few small bits.

using UnityEngine;
using System.Collections;

public class SpawnScript : MonoBehaviour
{
	public static SpawnScript shared;
	//DontDestroyOnLoad(this.gameObject);
	
	public GameObject PlayerPrefab;
	public GameObject JoysticsGO;

	#region Dynamic parameters

	public ClassName currentPlayerClassIndex = 0;
	public Vector2 playerClassBoxSize = new Vector2(100,100);

	public int team = 0;

	#endregion

	public GameObject myPlayer;
	
	public float WaitRespawnForSeconds { get { return GameSettings.respawnDelay; } }
	public Color[] playerTeamColors;
	private GameObject[] spawnPoints;
	private Vector2 screenScale;
	public float timeToRespawn;
	
	//private GameObject PauseMenuGO;

	void Awake()
	{
		if (shared) {
			Destroy (this);
			return;
		} 
		else 
			shared = this;
	}

	void OnDisconnectedFromServer()
	{
		if(myPlayer)
			Destroy(myPlayer);

		timeToRespawn = 0;

		foreach(GameObject go in GameObject.FindGameObjectsWithTag("Player"))
		{
			Destroy(go);
		}
	}

	void  Start ()
	{
		screenScale.x = Screen.width/1920.0f;
		screenScale.y = Screen.height/1080.0f;
		playerClassBoxSize.x *= screenScale.x;
		playerClassBoxSize.y *= screenScale.y;

		timeToRespawn = 0; //Last = 1;

		spawnPoints = GameObject.FindGameObjectsWithTag("PlayerSpawnPoint");
		//print ("sp "+spawnPoints.Length);
	}
	
	void  Update ()
	{
		if(!myPlayer && (Network.isClient || Network.isServer))
		{
			//InGameMenu.shared.isPaused = true;
			//PauseMenuGO.GetComponent<IGMenu>().isPaused = true;
			
			if(timeToRespawn > 0)
			{
				timeToRespawn -= Time.deltaTime;
			}

		}

	}
	public void TryTrySpawn()
	{
		Debug.Log ("spawn");
		if(timeToRespawn < 1)
		{
			TrySpawn();
		}
	}
	/*void  OnGUI ()
	{
		if(!myPlayer && (Network.isClient || Network.isServer))// && MainMenuScript.shared.currentMenu == MainMenuScript.MenuType.InGame)
		{
			//MainMenuScript.shared.mySkinId = int.Parse( GUI.TextField(new Rect(Screen.width/2 - 100, Screen.height/2 - 67.5f, 50, 20), MainMenuScript.shared.mySkinId.ToString()) );
			if(GUI.Button( new Rect(Screen.width/2 - 100, Screen.height/2 - 37.5f, 200, 75), "Spawn"+( timeToRespawn >= 1 ? " "+(int)(timeToRespawn) : "")))
			{
				TryTrySpawn();
			}

			int n = PlayerClasses.shared.classes.Count;
			GUILayout.BeginArea(new Rect((Screen.width - n*playerClassBoxSize.x)/2, Screen.height - 100*screenScale.y - playerClassBoxSize.y, n*playerClassBoxSize.x, playerClassBoxSize.y));
			GUILayout.BeginHorizontal();
			for(int i = 0; i < PlayerClasses.shared.classes.Count; i++)
			{
				PlayerClass c = PlayerClasses.shared.classes[i];
				if(i == (int)currentPlayerClassIndex)
					GUILayout.Box(c.name);
				else if(GUILayout.Button(c.name))
				{
					currentPlayerClassIndex = (ClassName)i;
				}
			}
			GUILayout.EndHorizontal();
			GUILayout.EndArea();
		}
	}*/

	#region called on client

	private void TrySpawn()
	{
		spawnPoints = GameObject.FindGameObjectsWithTag("PlayerSpawnPoint");
		
		//spawnPoints = GameObject.FindGameObjectsWithTag("PlayerSpawnPoint"+NetworkManager.MyInfo.team);
		int rand = Random.Range(0, spawnPoints.Length);
		Spawn(rand, spawnPoints[rand].transform.position);
	}
	
	private void Spawn(int spawnPointID, Vector3 pos)
	{
		timeToRespawn = WaitRespawnForSeconds;
		spawnPointID = Mathf.Clamp(spawnPointID, 0, spawnPoints.Length);

		myPlayer = Network.Instantiate(PlayerPrefab, (Vector3)pos, spawnPoints[spawnPointID].transform.rotation, 0) as GameObject;

		if(Network.isServer)
			Game.shared.OnPlayerSpawned(0);
		else
			Game.shared.GetComponent<NetworkView>().RPC ("OnPlayerSpawned", RPCMode.Server, NetworkManager.MyInfo.id);

		NetworkManager.shared.leftControls.SetActive(true);
		NetworkManager.shared.rightControls.SetActive(true);
		SetSpawned (true);
	}
	public bool isSpawned;
	public void SetSpawned(bool isSpawned)
	{
		this.isSpawned = isSpawned;
		if(!JoysticsGO)return;
		JoysticsGO.SetActive (isSpawned);
		DrawClasses.shared.EnablePanel (!isSpawned);
	}

	#endregion
	
	void OnPlayerDisconnected(NetworkPlayer player)
	{
		Debug.Log("Clean up after player " + player);
		Network.RemoveRPCs(player);
		Network.DestroyPlayerObjects(player);
	}
}