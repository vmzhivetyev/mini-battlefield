﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour 
{
	public float moveSpeed { get { return PlayerClasses.shared.classes[(int)GetComponent<Player>().currentClass].movementSpeed; } }
	public PlayerSoundSystem soundSystem { get { return GetComponent<PlayerSoundSystem>(); } }

	public float x,y;

	private float maxVelocityChange = 0.04f;

	public void JoystickMove( MovingJoystick move)
	{
		if(move.joystickName != "MovingJoystick")return;
		x = Mathf.Clamp(move.joystickAxis.x * 2, -1, 1);
		y = Mathf.Clamp(move.joystickAxis.y * 2, -1, 1);
	}

	public void JoystickMoveEnd( MovingJoystick move)
	{
		if(move.joystickName != "MovingJoystick")return;
		x = 0; y = 0;
	}
	
	void Start () 
	{
		if (!GetComponent<NetworkView>().isMine || GetComponent<Player>().isBot)
		{
			//enabled = false;
			return;
		}
		EasyJoystick.On_JoystickMove += JoystickMove;
		EasyJoystick.On_JoystickMoveEnd += JoystickMoveEnd;
	}
	
	void FixedUpdate () 
	{
		if (GetComponent<NetworkView>().isMine && !GetComponent<Player>().isBot)
		{
			if(Input.GetAxis ("Horizontal") != 0 || Input.GetAxis ("Vertical") != 0)
				NetworkManager.shared.ezjoystick.enabled = false;

			if(!NetworkManager.shared.ezjoystick.enabled)
			{
				x = Input.GetAxis ("Horizontal");
				y = Input.GetAxis ("Vertical");
			}
		}

		var targetVelocity = new Vector2 (x, y).normalized;
		//if (targetVelocity != Vector2.zero) 
		{
			if(!GetComponent<ShootingController>().isRotating && targetVelocity != Vector2.zero)
				transform.LookAt (transform.position + new Vector3 (0, 0, 1), new Vector3 (x, y, 0));
			//rigidbody2D.drag=0;
			//rigidbody2D.AddForce((dir * moveSpeed - rigidbody2D.velocity)*rigidbody2D.mass);
			//rigidbody2D.velocity = dir * moveSpeed;


			targetVelocity *= moveSpeed;

			var velocity = GetComponent<Rigidbody2D>().velocity;
			var velocityChange = (targetVelocity - velocity);
			velocityChange.x = Mathf.Clamp(velocityChange.x, -maxVelocityChange, maxVelocityChange);
			velocityChange.y = Mathf.Clamp(velocityChange.y, -maxVelocityChange, maxVelocityChange);
			GetComponent<Rigidbody2D>().AddForce(velocityChange*GetComponent<Rigidbody2D>().mass/Time.fixedDeltaTime);
		}
		/*
		else 
		{
			rigidbody2D.drag = Mathf.Lerp(rigidbody2D.drag, 80, 5*Time.fixedDeltaTime);
		}
		*/
	}

	void OnDestroy()
	{
		if(!GetComponent<NetworkView>().isMine || GetComponent<Player>().isBot)return;
		EasyJoystick.On_JoystickMove -= JoystickMove;
		EasyJoystick.On_JoystickMoveEnd -= JoystickMoveEnd;
	}
}
