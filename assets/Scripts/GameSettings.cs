﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameSettings : MonoBehaviour 
{
	public static GameSettings shared;

	#region synchronizable
	//public GameModeName gameMode = (GameModeName)0;

	public static float timeToBeginRegen =3;

	public static float hpRegenSpeed = 1;

	public static float respawnDelay = 3;

	public static float vehicleRespawnTime = 5;

	public static float DefaultCamOrtoSize = 6;
	#endregion

	public static Vector3 RotateAroundAxis(Vector3 v,float a,Vector3 axis, bool bUseRadians = false)
	{
		if (bUseRadians) a *= Mathf.Rad2Deg;
		var q = Quaternion.AngleAxis(a, axis);
		return q * v;
	}

	void Awake() 
	{
		if (shared) {
			Destroy (this);
			return;
		} 
		else 
			shared = this;

		Application.targetFrameRate = 60;

		EventSystem.OnGameLoaded += OnGameLoaded;
		
	}

	void OnGameLoaded()
	{
		Camera.main.orthographicSize = GameSettings.DefaultCamOrtoSize;
	}
}
