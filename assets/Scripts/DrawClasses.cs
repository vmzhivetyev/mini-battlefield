﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;


public class DrawClasses : MonoBehaviour 
{
	public static DrawClasses shared;
	public GameObject panel;
	public GameObject ButtonPrefab;
	public GameObject SpawnButton;
	public Text Counter;
	List<Button> buttons = new List<Button>();

	void Start()
	{
		shared = this;
		SetClasses ();
		SpawnButton = GameObject.Find ("SpawnButton");
		SpawnButton.GetComponent<Button>().onClick.AddListener(()=>
	    {
			Spawn((int)SpawnScript.shared.currentPlayerClassIndex);
		});
	}

	public void EnablePanel(bool isEnable)
	{
		if(!SpawnButton)return;

		SpawnButton.SetActive (isEnable);
		SpawnButton.GetComponent<Button> ().interactable = SpawnScript.shared.timeToRespawn <= 0;

		panel.SetActive (false);
		//panel.SetActive (isEnable);
		Counter.gameObject.SetActive (isEnable && SpawnScript.shared.timeToRespawn > 0);
		if(Counter.gameObject.activeInHierarchy)
		{
			SetCounter(string.Format("{0:0.00}",SpawnScript.shared.timeToRespawn));
		}
		//Counter.gameObject.SetActive (false);
	}

	void SetClasses()
	{
		float k = 1.0f / PlayerClasses.shared.classes.Count;
		for(int i = 0; i < PlayerClasses.shared.classes.Count; i++)
		{
			PlayerClass c = PlayerClasses.shared.classes[i];

			GameObject butt = Instantiate(ButtonPrefab) as GameObject;
			var rt =butt.GetComponent<RectTransform> ();
			var text =butt.GetComponentInChildren<Text> ();
			text.text=c.name;
			rt.SetParent(panel.transform);
			//butt.transform.parent = panel.transform;
			Vector2 vec = rt.anchorMax;
			vec.y=1-i*k;
			rt.anchorMax=vec;
			vec = rt.anchorMin;
			vec.y=rt.anchorMax.y - k;
			rt.anchorMin=vec;
			vec = rt.sizeDelta;
			vec.y=-10;
			vec.x=0;
			rt.sizeDelta=vec;
			vec = rt.anchoredPosition;
			vec.y=0;
			vec.x=0;
			rt.anchoredPosition=vec;
			butt.name=i.ToString();

			Button bt = butt.GetComponent<Button>();
			bt.onClick.AddListener(()=>
			{
				Spawn(int.Parse(butt.name));
			});
			buttons.Add(bt);
		}
	}

	public void Spawn(int classID)
	{		
		SpawnScript.shared.currentPlayerClassIndex = (ClassName)classID;
		SpawnScript.shared.TryTrySpawn ();
	}

	public void Update()
	{
		DrawClasses.shared.EnablePanel (!SpawnScript.shared.isSpawned && Game.state==GameState.InGame);
		
		//DrawClasses.shared.SetCounter (SpawnScript.shared.timeToRespawn);
	}

	public void SetCounter(string s)
	{
		//if (timer <= 0) return;
		//Debug.Log (string.Format ("{0:0.00}", timer));
		//string s = string.Format("{0:0.00}",timer);
		Counter.text = s;
		//buttons.ForEach((x)=>{x.interactable=s=="";});
	}
}
