﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

[System.Serializable]
public struct NetworkPlayerInfo
{
	public string nickname;
	public int id;
	public int kills;
	public int deaths;
	public string ip;
	public int score;
	public int team;
	public bool isBot;
	public GameObject gameObject;
}

public class NetworkManager : MonoBehaviour 
{
	public static NetworkManager shared;

	public static NetworkPlayerInfo MyInfo
	{
		get
		{
			return new NetworkPlayerInfo
			{ 
				nickname = shared.Nickname, 
				id = int.Parse(Network.player.ToString()),
				ip = Network.player.externalIP,
				team = shared.team
			};
		}
	}

	public string Nickname = "Name";
	public int team { get { return SpawnScript.shared.team; } }
	public string connectToIP = "127.0.0.1";
	public int connectPort = 25001;
	public bool canStartServer = true;
	//public string MainMenuLvl = "";
	//public string gameLevel = "MainScene";

	public bool localhostAsDefault = true;
	public bool autoConnect = true;

	public EasyJoystick ezjoystick;
	public GameObject leftControls;
	public GameObject rightControls;
	public GameObject enterExitBtn;

	public List<NetworkPlayerInfo> PlayerList = new List<NetworkPlayerInfo>();

	private int getTeamForNewPlayer { get { return PlayerList.Count()%2; } }
	private bool waitingForListUpdate = false;
	private int waitingForListUpdateID = -1;

	//private int lastLevelPrefix = 0;

	public static bool autoServer;

	void Start()
	{
		if (Network.isServer) OnServerInitialized ();
		if (autoServer) {
			Invoke("StartServer",1);
			return;
		}
/*
#if UNITY_EDITOR
		if(!canStartServer)
		{
			Network.InitializeServer(32, connectPort);
		}
		canStartServer = true;
#endif
#if UNITY_STANDALONE_WIN && !UNITY_EDITOR
		if(localhostAsDefault)
			connectToIP = "127.0.0.1";

		if(autoConnect)
			Network.Connect(connectToIP, connectPort);
#endif
*/
		//OnServerInitialized ();
	}

	public void StartServer()
	{
		Network.InitializeServer(32, connectPort);
	}

	public void ResetStats()
	{
		foreach(NetworkPlayerInfo p in PlayerList)
		{
			UpdatePlayerStats(p.id, -p.kills, -p.deaths, -p.score);
		}
	}

	[RPC] 
	void ChangeTeam(int id, int _team)
	{
		//print ("set " + team + " team for player #" + id);
		if(id != MyInfo.id || MyInfo.team == _team)return;
		if(SpawnScript.shared.myPlayer)
		{
			SpawnScript.shared.myPlayer.SendMessage ("SetDamagerID", -2);
			SpawnScript.shared.myPlayer.SendMessage ("ApplyDamage", 1000);
		}
		SpawnScript.shared.team = _team;
		NetworkPlayerInfo m = PlayerList.Find(x => x.id == id);
		GetComponent<NetworkView>().RPC("UpdateStats", RPCMode.AllBuffered, m.nickname, id, 0, 0, 0, MyInfo.team);
	}
		
	[RPC]
	void ReceiveNewPlayerInfo(string name, int id, string ip, int team)
	{
		//Debug.Log ("CALL THAT FUCKING FUNCTION!!!!!");
		//if (id == MyInfo.id)
		//	SpawnScript.shared.team = team;
		PlayerList.Add(new NetworkPlayerInfo{nickname = name, id = id, kills = 0, deaths = 0, ip = ip, team = team, isBot=ip=="_bot"});
		GetComponent<GUIPlayerList>().Create(id);
		//waitingForListUpdate = false;

		//if(Network.isServer)
		//	networkView.RPC("ChangeTeam", RPCMode.All, id, getTeamForNewPlayer);
		//Bot.CheckBotsCount();
	}

	[RPC]
	void PlayerLeft(int id)
	{
		int _index = PlayerList.FindIndex (x => x.id == id);
		if(_index < 0)return;

		PlayerList.RemoveAt(_index);

		if(id == waitingForListUpdateID)
			waitingForListUpdate = false;
		GetComponent<GUIPlayerList>().Delete(id);
		//Bot.CheckBotsCount();
	}

	public void UpdatePlayerStats (int id, int dkills, int ddeaths, int dscore)
	{
		NetworkPlayerInfo m = PlayerList.Find(x => x.id == id);
		GetComponent<NetworkView>().RPC("UpdateStats", RPCMode.AllBuffered, m.nickname, m.id, dkills, ddeaths, dscore, m.team);
	}

	public void GiveScore(int id, int score, string reason = "")
	{
		if(score == 0)return;
		UpdatePlayerStats (id, 0, 0, score);
		if(reason != "") GetComponent<NetworkView>().RPC ("ShowScoreMessage", RPCMode.All, id, reason, score);
	}

	[RPC]
	public void ShowScoreMessage(int id, string msg, int score)
	{
		if(id != MyInfo.id)return;
		NotificationSystem.shared.ShowMessage (msg + " " + score);
	}

	[RPC]
	void UpdateStats(string name, int id, int dkills, int ddeaths, int dscore, int team)
	{
		//Debug.Log("UpdateStats #"+id+" with team "+team);
		if(PlayerList.FindIndex(x => x.id == id) == -1)
		{
			return; // because its server's stats
		}
		NetworkPlayerInfo inf = PlayerList.Find(x => x.id == id);
		inf.nickname = name;
		inf.kills += dkills;
		inf.deaths += ddeaths;
		inf.score += dscore;
		inf.team = team;
		PlayerList[PlayerList.FindIndex(x => x.id == id)] = inf;
		if(id == waitingForListUpdateID)
			waitingForListUpdate = false;
	}

	[RPC]
	void OnKill(int victimId, int killerId)
	{
		NetworkPlayerInfo vic = PlayerList.Find(x => x.id == victimId);
		UpdateStats(vic.nickname, victimId, 0, 1, 0, vic.team);
		int killerIndex = PlayerList.FindIndex(x => x.id == killerId);
		if(killerIndex == -1)return;
		NetworkPlayerInfo kil = PlayerList[killerIndex];
		if(victimId != killerId) 
		{
			UpdateStats(kil.nickname, killerId, 1, 0, 0, kil.team);
			if(killerId == MyInfo.id || (Network.isServer && kil.isBot))
			{
				GiveScore(killerId, Game.current.pointsForKill, kil.isBot ? "" : "Player killed");
			}
		}
	}

	void Awake()
	{
		if(shared){
			Destroy(gameObject);
			return;
		}
		else
		{
			shared = this;
			DontDestroyOnLoad(gameObject);
		}
	}
	
	//Client functions called by Unity
	void OnConnectedToServer ()
	{
		Debug.Log("This CLIENT has connected to a server");
		if(Nickname=="Name")  Nickname = "Player " + MyInfo.id;
		GetComponent<NetworkView>().RPC("ReceiveNewPlayerInfo", RPCMode.AllBuffered, Nickname, MyInfo.id, MyInfo.ip, MyInfo.team);
	}
		
	void OnDisconnectedFromServer ( NetworkDisconnection info  )
	{
		Debug.Log("This SERVER OR CLIENT has disconnected from a server");
		Network.RemoveRPCs(Network.player);

		PlayerList.Clear ();
		GetComponent<GUIPlayerList> ().Clear ();

		foreach(GameObject g in GameObject.FindGameObjectsWithTag("Bullet"))
		Destroy(g);
		foreach(GameObject g in GameObject.FindGameObjectsWithTag("Car"))
		Destroy(g);
	}

	public void DestroyGameObjects()
	{
		foreach(GameObject g in GameObject.FindGameObjectsWithTag("Bullet"))
			Destroy(g);
		foreach(GameObject g in GameObject.FindGameObjectsWithTag("Car"))
			Destroy(g);
	}
		
	void OnFailedToConnect ( NetworkConnectionError error  ){
		Debug.Log("Could not connect to server: "+ error);
	}
		
		
	//Server functions called by Unity
	void  OnPlayerConnected ( NetworkPlayer player  )
	{
		Debug.Log("Player connected from: " + player.ipAddress +":" + player.port);
	}

		
	void OnServerInitialized ()
	{
		Debug.Log("Server initialized and ready");
		Debug.Log ("DO THat SHIT");
		if(Nickname=="Name") Nickname = "Player " + MyInfo.id;
		GetComponent<NetworkView>().RPC("ReceiveNewPlayerInfo", RPCMode.AllBuffered, Nickname, MyInfo.id, MyInfo.ip, MyInfo.team);

		NetworkMan.shared.LoadLevel("");

		//EventSystem.GameStarted();
		//LoadLevel (gameLevel);
	}
		
	void  OnPlayerDisconnected ( NetworkPlayer player  )
	{
		Debug.Log("Player disconnected from: " + player.ipAddress+":" + player.port);
		Chat.shared.TellLeave(int.Parse(player.ToString()));
		GetComponent<NetworkView>().RPC("PlayerLeft", RPCMode.AllBuffered, int.Parse(player.ToString()));
	
		/*foreach(GameObject g in GameObject.FindGameObjectsWithTag("Bullet"))
			if(g.GetComponent<BulletScript>().shooterId == int.Parse(player.ToString())) 
			{	
				Network.RemoveRPCs(g.networkView.viewID);
				Network.Destroy(g);
			}*/
	}
		
		
	// OTHERS:
	// To have a full overview of all network functions called by unity
	// the next four have been added here too, but they can be ignored for now
		
	void  OnFailedToConnectToMasterServer ( NetworkConnectionError info  ){
		Debug.Log("Could not connect to master server: "+ info);
	}
		
	void  OnNetworkInstantiate ( NetworkMessageInfo info  ){
		Debug.Log("New object instantiated by " + info.sender);
	}
		
	void  Update ()
	{
		if(!Network.isServer)return;
		if(waitingForListUpdate)return;
	}
}
