﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class CarEnter : MonoBehaviour 
{
	public float rayLength = 2;
	public LayerMask layerMask;
	[HideInInspector]
	public GameObject inCar = null;
	public CarNetwork cn;

	void OnSerializeNetworkView(BitStream stream, NetworkMessageInfo info) 
	{
		if(GetComponent<Player>().isBot || !inCar)return;

		if(stream.isWriting)
		{
			Vector3 v = (Vector3)inCar.GetComponent<Rigidbody2D>().velocity;
			stream.Serialize(ref v);
			///Debug.Log("Send car speed " + v);
		}
		else
		{
			stream.Serialize(ref inCar.GetComponent<CarNetwork>().carSpeed);
			//Debug.Log("Receive car speed " + cn.carSpeed);
		}
	}

	void Start()
	{
		if(!GetComponent<NetworkView>().isMine || GetComponent<Player>().isBot)
			enabled=false;
		else
			EasyButton.On_ButtonDown += OnBtnPressed;
	}

	void Update()
	{
		if(!inCar)
		{
			Vector3 dir = transform.up;
			Vector3 pos = transform.position;
			RaycastHit2D h = Physics2D.Raycast (pos + dir*((CircleCollider2D)GetComponent<Collider2D>()).radius, dir, rayLength - ((CircleCollider2D)GetComponent<Collider2D>()).radius, layerMask.value);

			if(!h || h.transform.tag != "Car")
			{
				cn = null;
			}
			else
			{
				//if(h.transform.gameObject.GetComponent<CarNetwork>().hasPassangers && h.transform.gameObject.GetComponent<CarNetwork>().ownerTeam != NetworkManager.MyInfo.id)
				//	cn = null;
				//else
					cn = h.transform.gameObject.GetComponent<CarNetwork>();
			}
		}

		NetworkManager.shared.enterExitBtn.SetActive(cn != null || inCar);
	}

	void OnDestroy()
	{
		if(GetComponent<NetworkView>().isMine)
			EasyButton.On_ButtonDown -= OnBtnPressed;
	}

	void OnBtnPressed (string name) 
	{
		if(name != "EnterOrExit")return;
		if(!inCar)
		{
			if(cn) 
			{
				if(cn.passengers.Count(x => x == null) > 0)
				{
					cn.Enter(gameObject);
				}
			}
		}
		else
		{
			inCar.GetComponent<CarNetwork>().Exit(gameObject);
		}
	}
}
