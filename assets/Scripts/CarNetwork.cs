﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class CarNetwork : MonoBehaviour 
{
	public List<GameObject> passengers = new List<GameObject>();
	public List<Transform> Places = new List<Transform>();
	public int secondsToStayWithoutPassengers=10;
	public float maxIdleRadius=1.5f;
	public float FOVMultiplier = 1.0f;

	public AudioClip[] sounds;


	private Vector2 startPosition;
	private float _secondsToStayWithoutPassengers = 0;
	[HideInInspector]
	public bool hasDriver = false;
	public int places { get { return Places.Count; } }
	public bool hasPassangers { get { return !passengers.TrueForAll(x => x == null); } }
	//public bool used { get { return startPosition != (Vector2)transform.position;}}
	public bool used { get { return (startPosition - (Vector2)transform.position).sqrMagnitude > maxIdleRadius * maxIdleRadius; } }
	public int ownerTeam { get { return hasPassangers ? passengers.Find(x => x != null).GetComponent<Player> ().team : -1; } }
	public Vector3 carSpeed;

	void Awake () 
	{
		hasDriver = false;
		passengers = new List<GameObject>();
		for(int i = 0; i < places; i++)
			passengers.Add(null);
	}
	void Start()
	{
		startPosition = (Vector2)transform.position;
	}

	void Update () 
	{
		if(!GetComponent<NetworkView>().isMine)return;
		if(hasDriver && passengers[0] == null)
		{
			GetComponent<NetworkView>().RPC("DriverLeaved", RPCMode.AllBuffered);
		}

		if (hasPassangers || !used) _secondsToStayWithoutPassengers = 0;
		if (_secondsToStayWithoutPassengers >= secondsToStayWithoutPassengers) 
		{
			var car = GetComponent<Car>();
			car.SetDamagerID(-1);
			car.ApplyDamage(car.healthPoints);
		}else  
			_secondsToStayWithoutPassengers+=Time.deltaTime;
	}

	public void Enter(GameObject go)
	{
		GetComponent<NetworkView>().RPC("EnterRPC", RPCMode.AllBuffered, go.GetComponent<Player>().id);
		GetComponent<NetworkView>().RPC ("PlaySound", RPCMode.All, 0);
	}

	public void Exit(GameObject go)
	{
		GetComponent<NetworkView>().RPC("ExitRPC", RPCMode.AllBuffered, go.GetComponent<Player>().id);
		GetComponent<NetworkView>().RPC ("PlaySound", RPCMode.All, 1);
	}

	[RPC]
	void PlaySound(int id)
	{
		GetComponent<AudioSource>().PlayOneShot (sounds [id]);
	}

	[RPC]
	void EnterRPC(int id)
	{
		bool driver = passengers[0] == null;
		GameObject player = NetworkManager.shared.PlayerList.Find(x => x.id == id).gameObject;

		player.GetComponent<Rigidbody2D>().isKinematic = true;
		player.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
		//player.collider2D.enabled = false;
		player.GetComponent<Collider2D>().isTrigger = true;
		player.layer = 2;

		int index = passengers.FindIndex(x => x == null);
		passengers[index] = player;

		player.transform.parent = Places[index].parent;
		player.transform.localPosition = Places[index].localPosition;
		player.transform.localRotation = Places[index].localRotation;
		player.GetComponent<PlayerController>().enabled = false;
		player.GetComponent<MovementUpdate>().newPosition = gameObject.transform.position;
		player.GetComponent<MovementUpdate>().newRotation = gameObject.transform.rotation;
		player.GetComponent<MovementUpdate>().go = gameObject;
		
		GetComponent<MovementUpdate>().enabled = false;

		if(driver)
			hasDriver = true;

		player.GetComponent<CarEnter>().inCar = gameObject;

		if(id == NetworkManager.MyInfo.id)
		{
			Camera.main.orthographicSize = FOVMultiplier * GameSettings.DefaultCamOrtoSize;
			Camera.main.GetComponent<CameraFollow>().goal = transform;
			gameObject.layer = 8;
			if(driver)
			{
				player.GetComponent<MovementUpdate>().go = gameObject;
				GetComponent<CarPhysics>().RegisterEvents();
			}
			else
			{
				NetworkManager.shared.leftControls.SetActive(false);
				NetworkManager.shared.rightControls.SetActive(true);
			}
		}

		if(!driver)
			player.GetComponent<MovementUpdate>().enabled = false;
	}

	[RPC]
	void ExitRPC(int id)
	{
		GameObject player = NetworkManager.shared.PlayerList.Find(x => x.id == id).gameObject;
		bool driver = passengers[0] == player;

		player.GetComponent<Rigidbody2D>().isKinematic = false;
		//player.collider2D.enabled = true;
		player.GetComponent<Collider2D>().isTrigger = false;
		player.layer = id == NetworkManager.MyInfo.id ? 8 : 0;

		int index = passengers.FindIndex(x => x == player);

		passengers[index] = null;
		player.transform.parent = null;
		player.GetComponent<MovementUpdate>().go = player;
		player.GetComponent<MovementUpdate>().enabled = true;


		if(driver)
		{
			hasDriver = false;
			GetComponent<MovementUpdate>().newPosition = transform.position;
			GetComponent<MovementUpdate>().newRotation = transform.rotation;
			GetComponent<MovementUpdate>().enabled = true;
		}

		player.GetComponent<CarEnter>().inCar = null;

		if(id == NetworkManager.MyInfo.id)
		{
			Camera.main.orthographicSize = GameSettings.DefaultCamOrtoSize;
			Camera.main.GetComponent<CameraFollow>().goal = player.transform;
			player.GetComponent<PlayerController>().enabled = true;
			gameObject.layer = 0;
			if(driver)
			{
				GetComponent<NetworkView>().RPC("SyncCarVelocity", RPCMode.All, transform.position, transform.rotation, (Vector3)GetComponent<Rigidbody2D>().velocity, GetComponent<Rigidbody2D>().angularVelocity);
				GetComponent<CarPhysics>().UnRegisterEvents();
			}
			NetworkManager.shared.rightControls.SetActive(true);
			NetworkManager.shared.leftControls.SetActive(true);
		}
	}

	[RPC]
	void DriverLeaved()
	{
		hasDriver = false;
		GetComponent<MovementUpdate>().newPosition = transform.position;
		GetComponent<MovementUpdate>().newRotation = transform.rotation;
		GetComponent<MovementUpdate>().enabled = true;
		GetComponent<CarPhysics>().UnRegisterEvents();
	}

	[RPC]
	void SyncCarVelocity(Vector3 pos, Quaternion q, Vector3 vel, float angVel)
	{
		if(!GetComponent<NetworkView>().isMine)return;

		GetComponent<Rigidbody2D>().isKinematic = false;
		transform.position = pos;
		transform.rotation = q;
		GetComponent<Rigidbody2D>().velocity = (Vector2)vel;
		GetComponent<Rigidbody2D>().angularVelocity = angVel;
		GetComponent<Rigidbody2D>().drag = GetComponent<CarPhysics>().linearDrag;
	}

	public bool IsPassenger(GameObject g)
	{
		return passengers.Any(x => x == g);
	}

	public bool IsDriver(GameObject g)
	{
		return passengers[0] == g;
	}
}
