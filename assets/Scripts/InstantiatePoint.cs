﻿using UnityEngine;
using System.Collections;

public class InstantiatePoint : MonoBehaviour 
{
	public GameObject prefab;
	public string pointName = "Point A";

	void Start () {
		GameObject g = Network.Instantiate (prefab, transform.position, transform.rotation, 2) as GameObject;
		g.name = pointName;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
