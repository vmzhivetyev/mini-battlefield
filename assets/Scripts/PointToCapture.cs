﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class PointToCapture : MonoBehaviour 
{
	public List<GameObject> pointObjects;
	public int ownerTeam = -1;
	public int biggestTeamInside = -1;
	public int c0,c1;
	public float ownPercentage = 0;
	public float lastOwnPercentage = 0;
	public bool wasCaptured = false;
	
	public Color[] ownColors;
	
	/*void OnSerializeNetworkView(BitStream stream, NetworkMessageInfo info) 
	{
		stream.Serialize(ref ownerTeam);
		stream.Serialize(ref ownPercentage);
		NotificationSystem.shared.ShowMessage ("Serializing point" + ownPercentage);
	}
	
	void OnGameStarted()
	{
		ownerTeam = -1;
		biggestTeamInside = -1;
		c0 = 0;
		c1 = 0;
		ownPercentage = 0;
		lastOwnPercentage = 0;
		wasCaptured = false;
	}
	
	void Awake()
	{
		EventSystem.OnGameStarted += OnGameStarted;
		gameObject.AddComponent<NetworkView>();
		networkView.observed = this;
		networkView.stateSynchronization = NetworkStateSynchronization.ReliableDeltaCompressed;
		networkView.enabled = true;
	}
	
	void Update () 
	{
		Color c = ownColors [ownerTeam < 0 ? 0 : (ownerTeam == NetworkManager.MyInfo.team ? 1 : 2)], cc = ownColors[0];
		GetComponent<SpriteRenderer> ().color = new Color (cc.r + (c.r - cc.r) * ownPercentage, cc.g + (c.g - cc.g) * ownPercentage, cc.b + (c.b - cc.b) * ownPercentage, cc.a + (c.a - cc.a) * ownPercentage);
		
		if(!Network.isServer)return;
		
		if(pointObjects.Any(x=> x==null))
		{
			pointObjects.Remove(null);
			UpdTeams();
		}
		
		if(biggestTeamInside == -1 && ownPercentage < 1)
		{
			ownPercentage = Mathf.Clamp01(ownPercentage - Time.deltaTime*GameSettings.flagCaptureSpeed*(wasCaptured ? -1 : 1));
			if(ownPercentage == 0) ownerTeam = -1;
		}
		else if(biggestTeamInside > -2)
		{
			if(biggestTeamInside != ownerTeam)
			{
				ownPercentage = Mathf.Clamp01(ownPercentage - Time.deltaTime*GameSettings.flagCaptureSpeed*Mathf.Abs(c0 - c1));
				if(ownPercentage == 0)
				{
					if(ownerTeam >= 0)
					{
						Game.shared.pointsCaptured[ownerTeam]--;
						pointObjects.Where(x => x.GetComponent<Player>().team == biggestTeamInside).ToList<GameObject>().ForEach(x => NetworkManager.shared.GiveScore(x.GetComponent<Player>().id, GameSettings.pointsForFlagNeutralization, "Point neutralized"));
					}
					ownerTeam = biggestTeamInside;
				}
			}
			else if(biggestTeamInside == ownerTeam)
			{
				ownPercentage = Mathf.Clamp01(ownPercentage + Time.deltaTime*GameSettings.flagCaptureSpeed*Mathf.Abs(c0 - c1));
			}
		}
		
		if(ownPercentage==1 && lastOwnPercentage<1)
			pointObjects.Where(x => x.GetComponent<Player>().team == ownerTeam).ToList<GameObject>().ForEach(x => NetworkManager.shared.GiveScore(x.GetComponent<Player>().id, GameSettings.pointsForFlagCapture, "Point captured"));
		
		if(ownPercentage==1 && !wasCaptured)
		{
			Game.shared.pointsCaptured[ownerTeam]++;
			wasCaptured = true;
		}
		if(ownPercentage==0 && wasCaptured)
		{
			//Game.shared.pointsCaptured[ownerTeam]--;
			wasCaptured = false;
		}
		
		lastOwnPercentage = ownPercentage;
	}
	
	void OnTriggerEnter2D(Collider2D col)
	{
		if(!Network.isServer)return;
		if(col.gameObject.transform.tag != "Player")return;
		if(pointObjects.Contains(col.gameObject))return;
		pointObjects.Add (col.gameObject);
		UpdTeams();
	}
	
	void OnTriggerExit2D(Collider2D col)
	{
		if(!Network.isServer)return;
		if(col.gameObject.transform.tag != "Player")return;
		if(!pointObjects.Contains(col.gameObject))return;
		pointObjects.Remove (col.gameObject);
		UpdTeams();
	}
	
	void UpdTeams()
	{
		c0 = pointObjects.Count (x => x.GetComponent<Player> ().team == 0);
		c1 = pointObjects.Count (x => x.GetComponent<Player> ().team == 1);
		biggestTeamInside = (c0 == c1 && c0 > 0)?-2: (c0 > c1 ? 0 : (c1 > c0 ? 1 : -1));
	}*/
}
