﻿using UnityEngine;
using System.Collections;

public class KnifeAttack : MonoBehaviour 
{
	public float attackDelay = 0.2f;
	public bool canAttack 
	{ 
		get { return GetComponent<ShootingController>().canAttack; } 
		set { GetComponent<ShootingController>().canAttack = value; } 
	}
	public float rayLength = 1;
	public float damage = 50;

	public LayerMask layerMask;
	
	void Start()
	{
		if(GetComponent<NetworkView>().isMine)
			EasyButton.On_ButtonDown += HandleOn_ButtonDown;
		else
			enabled = false;
	}

	void HandleOn_ButtonDown (string buttonName)
	{
		if(buttonName != "KnifeButton" || GetComponent<Player>().isBot)return;
		if(canAttack)
		{
			canAttack = false;
			GetComponent<AnimationController>().AttackKnife = true;

			Invoke("Attack",attackDelay);
			Invoke("ResetAnim",0.1f);

			//canAttack = false;
			//GetComponent<AnimationController>().AttackKnife = true;
		}
	}

	void ResetAnim()
	{
		GetComponent<AnimationController>().AttackKnife = false;
	}
	
	void Attack()
	{
		Vector3 pos = transform.position;
		Vector3 dir = transform.up;
		Vector3 pos2 = transform.position - transform.right*((CircleCollider2D)GetComponent<Collider2D>()).radius;
		Vector3 pos3 = transform.position + transform.right*((CircleCollider2D)GetComponent<Collider2D>()).radius;
		
		//Debug.Log(Physics2D.Raycast (lastPos, dir, Vector2.Distance (pos, lastPos), layerMask.value) == true);
		RaycastHit2D h1 = (Physics2D.Raycast (pos, dir, rayLength, layerMask.value));
		RaycastHit2D h2 = (Physics2D.Raycast (pos2, dir, rayLength*0.75f, layerMask.value));
		RaycastHit2D h3 = (Physics2D.Raycast (pos3, dir, rayLength*0.75f, layerMask.value));
		if(h1 && h1.transform.tag == "Player")Hit(h1);
		else if(h2 && h2.transform.tag == "Player")Hit(h2);
		else if(h3 && h3.transform.tag == "Player")Hit(h3);
		else 
		{
			if(h1) Hit (h1); else
				if(h2) Hit (h2); else
					if(h3) Hit (h3);
		}
		canAttack=true;
	}

	void Hit(RaycastHit2D h)
	{
		if(h.transform.tag == "Player")
		{
			if(h.transform.gameObject == SpawnScript.shared.myPlayer)
				return;
		}
		Vector2 contact = h.point;
		float angle = Vector2.Angle(h.transform.up, transform.position - h.transform.position);
		float d = damage;
		if(h.transform.tag == "Player" && angle > 100)
		{
			d = 1000;
			GetComponent<NetworkView>().RPC("FKYou", RPCMode.All);
		}
		h.collider.SendMessage("SetDamagerID", GetComponent<Player>().id, SendMessageOptions.DontRequireReceiver);
		h.collider.SendMessage("ApplyDamage" ,        	               d, SendMessageOptions.DontRequireReceiver);
	}

	/*void Update()
	{
		Vector3 pos2 = transform.position + transform.right*((CircleCollider2D)collider2D).radius;
		Vector3 pos3 = transform.position - transform.right*((CircleCollider2D)collider2D).radius;
		Debug.DrawLine(transform.position, transform.position + transform.up*rayLength);
		Debug.DrawLine(pos2, pos2 + transform.up*rayLength*0.75f);
		Debug.DrawLine(pos3, pos3 + transform.up*rayLength*0.75f);
	}*/

	void OnDestroy()
	{
		if(GetComponent<NetworkView>().isMine)
			EasyButton.On_ButtonDown -= HandleOn_ButtonDown;
	}
}
