﻿using UnityEngine;
using System.Collections;

public class AmbientScript : MonoBehaviour 
{
	public float smoothStart;
	public float smoothEnd;
	public float defVolume = 1;

	void Update()
	{
		if(GetComponent<AudioSource>().time < smoothStart)
		{
			GetComponent<AudioSource>().volume = GetComponent<AudioSource>().time / smoothStart;
		}

		if(GetComponent<AudioSource>().time > GetComponent<AudioSource>().clip.length - smoothEnd)
		{
			GetComponent<AudioSource>().volume =  defVolume * ((GetComponent<AudioSource>().clip.length - GetComponent<AudioSource>().time) / smoothEnd);
		}
	}
}
