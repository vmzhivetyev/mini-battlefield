﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ShowDebugLogs : MonoBehaviour 
{
	[System.Serializable]
	public struct LogMsg
	{
		public int sender;
		public string log;
		public string stack;
		public LogType type;
	}

	public string output = "";
	public string stack = "";
	public int count = 0;

	public List<LogMsg> remoteLogs;

	[RPC]
	void ReceiveLog(int _id, string _output, string _stack, int _type)
	{
		output += "\r\n<" + ((LogType)_type) + " on " + _id + "> " + _output + "\r\n" + _stack + "\r\n\r\n";
		//remoteLogs.Add (new LogMsg{log = _output, sender = _id, stack = _stack, type = (LogType)_type});
		/*
		switch(_type)
		{
		case (int)LogType.Error:
			Debug.LogError(_output+ _stack);
			break;
		case (int)LogType.Exception:
			Debug.LogError(_output+ _stack);
			break;
		case (int)LogType.Log:
			Debug.Log(_output+ _stack);
			break;
		default:
			output = "fail";
			break;
		}*/
	}

	void Start() 
	{
		Application.RegisterLogCallback(HandleLog);
	}

	/*void OnDisable() 
	{
		Application.RegisterLogCallback(null);
	}*/

	void HandleLog(string logString, string stackTrace, LogType type) 
	{
		//if(Network.isServer)
		//	ReceiveLog(NetworkManager.MyInfo.id, logString, stackTrace, (int)type);
		//else
			GetComponent<NetworkView>().RPC("ReceiveLog", RPCMode.All, NetworkManager.MyInfo.id, logString, stackTrace, (int)type);
	}
}
