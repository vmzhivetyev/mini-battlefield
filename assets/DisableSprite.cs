﻿using UnityEngine;
using System.Collections;

public class DisableSprite : MonoBehaviour 
{
	public float time = 1.35f;
	void Start () 
	{
		Invoke("Disable", time);
	}
	
	void Disable () 
	{
		GetComponent<Renderer>().enabled = false;
	}
}
