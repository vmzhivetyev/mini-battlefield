﻿using UnityEngine;
using System.Collections;

public class AIPathTest : MonoBehaviour {

	public Transform target;
	public bool regen = true;
	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		if (!regen)
						return;
		regen = false;
		Seeker s = GetComponent<Seeker>();
		s.StartPath (transform.position, target.position);
	}
}
